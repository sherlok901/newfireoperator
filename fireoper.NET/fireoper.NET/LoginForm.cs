﻿using System;
using System.Linq;
using System.Threading;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Data.SqlClient;
using fireoper.NET.SharedClasses;

namespace fireoper.NET
{
    /// <summary>
    /// Форма логина
    /// </summary>
    public partial class LoginForm : Form
    {
        private string _login;
        private string _password;
        private string _dataSource;
        private string _dbName ;
        private FireLinqDataContext fl;
        private string _connectionString;
        private bool IsUserLogged = false;
        private bool IsError = false;
        
        public LoginForm()
        {
            InitializeComponent();
            // перевірити наявність settings.ini
            _dataSource = GetDataSource();
            if(string.IsNullOrEmpty(_dataSource))
                UserMessage.ShowMessage("Не знайдено settings.ini файл!");
            _dbName = GetDbName();

            this.StartPosition = FormStartPosition.CenterScreen;
            //entered login,password for debug
#if DEBUG
            textBox1.Text = "Admin";
            maskedTextBox1.Text = "adminadmin";
#endif
        }

        

        public string GetConnectionString()
        {
            return _connectionString;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        // клік ОК
        private void button1_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(textBox1.Text) || string.IsNullOrEmpty(maskedTextBox1.Text))
            {
                UserMessage.ShowMessage("Логін/пароль не можуть бути пустими!"); 
                return;
            }
         
            _login = textBox1.Text;
            _password = maskedTextBox1.Text;

            //Ці символи ТОЧНО небезпечні в рядку підключення.
            if (_password.IndexOfAny(new char[] { '=', '\"', ';' , '\\'}) != -1)
            {
                UserMessage.ShowMessage("Дані символи не дозволені!");
                return;
            }

            _connectionString = "Data Source=" + _dataSource + ";Initial Catalog=" + _dbName + ";Persist Security Info=True;User ID=" + _login + ";Password=" + _password;
            
            // Пробуємо підключитись до бд
            SqlConnection conn = new SqlConnection();
            try
            {
                conn.ConnectionString = _connectionString;
                conn.Open();
                conn.Close();
            }
            catch (ArgumentException ex)
            {
                //Got wrong arguments in connection string. :-(
                //Logs.writeToLogWindow("Exseption:" + ex.ToString() + "; stake:" + ex.StackTrace);
                IsError = true;
                UserMessage.ShowMessage("Невірний логін/пароль або з'єднання з базою даних!"+ex.StackTrace+";"+ex.Message+";"+ex.Source);
            }
            catch (SqlException ex)
            {
                //Logs.writeToLogWindow("Exseption:" + ex.ToString() + "; stake:" + ex.StackTrace);
                IsError = true;
                UserMessage.ShowMessage("Невірний логін/пароль або з'єднання з базою даних!" + ex.StackTrace + ";" + ex.Message + ";" + ex.Source);
            }
            catch (InvalidOperationException ex)
            {
                //Logs.writeToLogWindow("Exseption:" + ex.ToString() + "; stake:" + ex.StackTrace);
                IsError = true;
                UserMessage.ShowMessage(string.Format("Message:{0}; Stack:{1}; Source:{2}; Help:{3}",ex.Message,ex.StackTrace,ex.Source,ex.HelpLink));
            }
            finally
            {
                if (!IsError)
                {
                    RunProgram(_connectionString,_login,_password);
                }

            }
        }

        private void RunProgram(string connectStr, string login, string password)
        {
            this.Hide();
            IsError = false;
            //save login
            DataAccess.UserLogin = login.ToLower();
            DataAccess.ConnectString = connectStr;

            // створимо id операторського місця
            // потрібен для ідентифікації користувачів які запустили операторське місце під різними логінами
            //int ProgramId = string.Format("{0}_{1}_ntc", login.ToLower(), password.ToLower()).GetHashCode();
            
            //перевірити чи вже залогінений користувач
            fl = new FireLinqDataContext(connectStr);
            
            //loggedUser loggedUser = GetLoggedUser(DataAccess.UserLogin);
            //if (loggedUser != null)
            //{
            //    UserMessage.ShowMessage("Користувач " + DataAccess.UserLogin + " вже аутентифікований!");
            //    IsUserLogged = true;
            //}
            //else
            {
                //fl.loggedUser.InsertOnSubmit(new loggedUser() { userName = DataAccess.UserLogin });
                //fl.SubmitChanges();

                Form1 fm = new Form1(connectStr);
                fm.ShowDialog();
                //DeleteLoggedUser();
                //Thread.Sleep(100);
                Close();
            }
        }
        private string GetDataSource()
        {
            String temp = new String(' ', 255);
            string path = Application.StartupPath + "\\settings.ini";
            Win32.GetPrivateProfileString("main", "DbServer", "", temp, 255, path);
            return temp.Remove(temp.IndexOf('\0'));
        }

        private string GetDbName()
        {
            String temp = new String(' ', 255);
            string path = Application.StartupPath + "\\settings.ini";
            Win32.GetPrivateProfileString("main", "db", "", temp, 255, path);
            return temp.Remove(temp.IndexOf('\0'));
        }

        private void DeleteLoggedUser()
        {
            // Выдалити користувача як залогованого
            if (fl == null) fl = new FireLinqDataContext(DataAccess.ConnectString);
            loggedUser loggedUser = GetLoggedUser(DataAccess.UserLogin);
            if (loggedUser == null) Close();

            fl.loggedUser.DeleteOnSubmit(loggedUser);
            fl.SubmitChanges();
        }

        private loggedUser GetLoggedUser(string login)
        {
            return fl.loggedUser.FirstOrDefault();
        }

        // Нажато Enter
        private void textBox1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter) button1_Click(null, EventArgs.Empty);
        }
        // Нажато Enter
        private void maskedTextBox1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter) button1_Click(null, EventArgs.Empty);
        }
    }
    public class Win32 // описание класса
    {
        //[DllImport("user32.dll", CharSet = CharSet.Auto)]
        //public static extern int MessageBox(int hWnd, String text, String caption, uint type);
        [DllImport("Kernel32.dll", CharSet = CharSet.Auto)]
        public static extern int GetPrivateProfileString(String sSection, String sKey, String sDefault,
            String sString, int iSize, String sFile);
        [DllImport("Kernel32.dll", CharSet = CharSet.Auto)]
        public static extern bool WritePrivateProfileString(String sSection, String sKey, String sString, String sFile);
    }
}
