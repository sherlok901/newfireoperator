﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using System.Diagnostics;   //for process
using System.Windows.Forms.Design;
using fireoper.NET.JournalSELECTS.CustomControls;
using fireoper.NET.AutoDozvon;
using System.Data.SqlClient;
using System.Security.Permissions;
using System.Threading;
using fireoper.NET.SharedClasses;
using System.Data.SqlTypes;
using System.Data;

namespace fireoper.NET
{
    public partial class Form1 : Form
    {
        
        SMSC smsc;
        AutoDozvonCtrl _autoDzvCntrl;
        FireLinqDataContext fl;
        FaultJournalControl _faultJornal;
        AlertJournalControl _alertJournal;
        BidJoutnalCtrl _bidJournalCtrl;
        DutyEmployersJournal _dutyEmpJournal;
        IQueryable<alarm_request_journal_view> _alarmReqJourList;
        FiresJournal _firesJournl;
        DutyJournalTechGroup _dutyJournalTechGroup;
        AlertsFaults _alertsFaults;
        QueueNotifications _queueNotifications;
        SqlConnection _connect;
        SqlCommand _command;
        SqlDependency _dependency;
        SqlDataAdapter da;
        DataSet ds;
        AutoDozvonController _AutoDozvonController=new AutoDozvonController();
        static object theLock = new object();
        AlertOptionsForm AlertOptionsForm_;
        private Thread newAlertsThread;
        

        public Form1(string ConnStr)
        {
            InitializeComponent();
            WindowState = FormWindowState.Maximized;
            StartPosition = FormStartPosition.CenterScreen;
            DataAccess.ConnectString = ConnStr;
            
            toolStripStatusLabel1.Text = DataAccess.UserLogin;
            fl=new FireLinqDataContext(ConnStr);
            smsc = new SMSC();

            _alarmReqJourList = from c in fl.alarm_request_journal_view orderby c.ReceiveDTStamp.Value descending select c;
            
            OpenSplitter();

             CheckAlertsChanges();
            AlertOptionsForm_ = new AlertOptionsForm(_AutoDozvonController);

            Options.GeneralOptions = fl.generalOptions.FirstOrDefault();
            Options.LinqTimeout = 60;
        }

        #region Слідкування за таблицею тривог в БД
        /// <summary>
        /// підписка на слідкування за таблицею тривог в БД
        /// </summary>
        void CheckAlertsChanges()
        {
            //check permission
            SqlClientPermission permission = new SqlClientPermission(PermissionState.Unrestricted);
            try { permission.Demand(); }
            catch (Exception) { }

            _connect = new SqlConnection(DataAccess.ConnectString);
            _connect.Open();
            _command = new SqlCommand(@"SELECT Id
                  ,ReceivedTime
                  ,ClosedTime
                  ,GuardObjectId
                  ,PremisesId
                  ,JobNo
                  ,EquipmentId
                  ,AlarmReasonId
                  ,AlarmCategoryId
                  ,msrepl_tran_version
                  ,rowguid
                  FROM dbo.[alarm_job_journal]", _connect);
            _dependency = new SqlDependency(_command);
            
            SqlDependency.Start(DataAccess.ConnectString);
            _dependency.OnChange += dependency_OnChange;
            SqlDataReader reader = _command.ExecuteReader();
        }

        //коли таблиця alarm_job_journal в якій зберігаються тривоги виникли зміні
        //викликається дана подія
        void dependency_OnChange(object sender, SqlNotificationEventArgs e)
        {
            //коли в таблицю доданий запис(додана тривога)
            if (e.Info.Equals(SqlNotificationInfo.Insert)
                && e.Source.Equals(SqlNotificationSource.Data)
                && e.Type.Equals(SqlNotificationType.Change))
            {
                // оповіщення про тривоги
                newAlertsThread = new Thread(Notify);
                newAlertsThread.IsBackground = true;
                newAlertsThread.Start();
            }

            //знову підписуємось на нові тривоги
            if (this.InvokeRequired)
                this.BeginInvoke(new MethodInvoker(CheckAlertsChanges));
            else
                CheckAlertsChanges();
            SqlDependency dep = sender as SqlDependency;
            dep.OnChange -= dependency_OnChange;
        }
        #endregion

        /// <summary>
        /// ВІдправляєм оповіщення
        /// </summary>
        public void Notify()
        {
            lock (theLock)
            {
                //если автодозвон откл
                if (!Options.GeneralOptions.emailAlertsNotification && !Options.GeneralOptions.smsAlertsNotification) return;

                NotifiedAlertManager notifiedManager = new NotifiedAlertManager();
                notifiedManager.Notify(Options.GeneralOptions.emailAlertsNotification,Options.GeneralOptions.smsAlertsNotification);
                //newAlertsThread.Abort();
            }
        }


        #region Відправка СМС, email
        private string[] SendSmsViaInet(string number, string text, int translit = 0)
        {
            string[] result = smsc.send_sms(number, text, translit);

            return result;
            /*
             * Результат функції треба аналізувати так:
            if (result.Length == 2)
            {
                //error occured
                "ID повід. " це result[0];
                "Код помилки " це result[1];
            }
            else
            {
                "Вартість " це result[2];
                "Баланс " це result[3];
            }*/
        }

        private string SendSmsViaAndroidPhone(string number, string text)
        {
            Process p = new Process
            {
                StartInfo =
                {
                    FileName = Application.StartupPath + "\\adb\\adb.exe",
                    Arguments = "devices",
                    //Arguments = "shell am start -a android.intent.action.SENDTO -d sms:+" + number.ToString() + " --es sms_body \"" + text + "\" --ez exit_on_sent true",
                    UseShellExecute = false,
                    CreateNoWindow = false,
                    RedirectStandardOutput = true
                }
            };
            p.Start();
            p.WaitForExit();
            string output = p.StandardOutput.ReadToEnd();
            if (!output.Equals("List of devices attached \r\n\r\n"))
            {
                //Якийсь девайс є
                p.StartInfo.Arguments = "shell am start -a android.intent.action.SENDTO -d sms:+" + number + " --es sms_body \"" + text + "\" --ez exit_on_sent true";
                p.Start();
                p.WaitForExit();
                p.StartInfo.Arguments = "shell input keyevent KEYCODE_TAB";
                p.Start();
                p.WaitForExit();
                p.StartInfo.Arguments = "shell input keyevent KEYCODE_DPAD_RIGHT";
                p.Start();
                p.WaitForExit();
                p.StartInfo.Arguments = "shell input keyevent KEYCODE_ENTER";
                p.Start();
                p.WaitForExit();

                return "Success!";
            }
            else
            {
                return "adb не бачить жодного Android телефона!";
            }
        }

        private void SendMail(string subject, string messageBody, string fromAddress, string domain, int port, string login, string pass, string toAddress, string ccAddress)
        {
            //SendEmail.SendMessage(subject, messageBody, fromAddress, domain, port, login, pass, toAddress, ccAddress);
        } 
        #endregion

        //Довідники/Журнали
        private void treeView1_NodeMouseDoubleClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            //Якщо є дочірні ноди, то відкривати закладку не треба.
            if (e.Node.GetNodeCount(true) > 0)
                return;

            //new datagridview
            DataGridView dg = new DataGridView();

            dg.ReadOnly = false;

            //adding tab
            TabPage tp = new TabPage(e.Node.Text + "     X");

            switch (e.Node.Name)
            {
                case "Node29":
                    PersonAlertOption pa = new PersonAlertOption(DataAccess.ConnectString);
                    BindingSource bs = new BindingSource();
                    bs.DataSource = pa.list;
                    
                    bs.CurrentItemChanged += new EventHandler(bs_CurrentItemChanged);

                    dg.EditMode = DataGridViewEditMode.EditOnEnter;
                    dg.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;

                    _autoDzvCntrl = new AutoDozvon.AutoDozvonCtrl();
                    _autoDzvCntrl.SendButton.Click += new EventHandler(SendButton_Click);
                    _autoDzvCntrl.dataGridView1.CellClick += new DataGridViewCellEventHandler(dataGridView1_CellClick);
                    _autoDzvCntrl.Dock = System.Windows.Forms.DockStyle.Fill;
                    _autoDzvCntrl.dataGridView1.DataSource = bs.DataSource;
                    
                    _autoDzvCntrl.dataGridView1.Columns[0].HeaderText = "Відповідальна особа";
                    _autoDzvCntrl.dataGridView1.Columns[2].HeaderText = "Мобільний телефон";
                    _autoDzvCntrl.dataGridView1.Columns[3].HeaderText = "Email оповіщення";
                    _autoDzvCntrl.dataGridView1.Columns[4].HeaderText = "Sms оповіщення";
                    tp.Controls.Add(_autoDzvCntrl);
                    
                    bs.EndEdit();
                    
                    break;
                case "Node1":   //Адресна інформація
                    dg.DataSource = from c in fl.addr_area select new { Назва = c.Name, Головний = c.ipMain, Резервний = c.ipReserve, Примітка = c.Note };
                    tp.Controls.Add(dg);
                    break;
                case "node22":  //Тип об'єктів
                    dg.DataSource = from c in fl.guard_object_kind select new { Назва = c.Name };
                    tp.Controls.Add(dg);
                    break;
                case "node23":  //Категорія об'єктів
                    dg.DataSource = from c in fl.guard_object_category select new { Назва = c.Name };
                    tp.Controls.Add(dg);
                    break;
                case "node24":
                    dg.DataSource = from c in fl.equipment_model where c.EquipmentTypeId == 7 select new { Назва = c.Name, Примітка = c.Note, c.IsAddress };
                    tp.Controls.Add(dg);
                    break;
                case "node25":
                    dg.DataSource = from c in fl.equipment_model where c.EquipmentTypeId == 4 select new { Назва = c.Name, Адресний = c.IsAddress, Примітка = c.Note };
                    tp.Controls.Add(dg);
                    break;
                case "node26":
                    dg.DataSource = from c in fl.equipment_model where c.EquipmentTypeId == 6 select new { Назва = c.Name, Адресний = c.IsAddress, Примітка = c.Note };
                    tp.Controls.Add(dg);
                    break;
                case "node27":
                    dg.DataSource = from c in fl.equipment_model where c.EquipmentTypeId == 8 select new { Сповіщувач = c.Name, Адресний = c.IsAddress, Примітка = c.Note };
                    tp.Controls.Add(dg);
                    break;
                case "Node8"://журнал пожеж
                    _firesJournl = new FiresJournal(DataAccess.ConnectString);
                    tp.Controls.Add(_firesJournl);
                    break;
                case "Node9"://Журнал несправностей
                    _faultJornal = new FaultJournalControl();
                    tp.Controls.Add(_faultJornal);
                    break;
                case "Node10"://Журнал тривог 
                    _alertJournal = new AlertJournalControl(DataAccess.ConnectString);
                    tp.Controls.Add(_alertJournal);
                    break;
                case "Node11"://Журнали->Сповіщення
                    List<alarm_request_journal_view> listVar = (from c in fl.alarm_request_journal_view orderby c.ReceiveDTStamp.Value descending select c).ToList();
                    dg.DataSource = listVar;
                    tp.Controls.Add(dg);
                    //DataGrid TestDgrid = new DataGrid();
                    //TestDgrid.Dock = DockStyle.Fill;
                    //TestDgrid.DataSource = AlarmReqJourList;
                    //tp.Controls.Add(TestDgrid);
                    break;
                case "Node12"://Журнали->Обєкти
                    dg.DataSource = from c in fl.guard_object_journal_view
                                    where c.StatusId != 5
                                    orderby c.GuardObjectNo
                                    select c;
                    tp.Controls.Add(dg);
                    break;
                case "Node13"://Журнали->Апаратура
                    dg.DataSource = from c in fl.equipment_journal_view orderby c.FullInstanceNo select c;
                    tp.Controls.Add(dg);
                    break;
                case "Node15"://Журнали->Журнали чергування співробітників   
                    _dutyEmpJournal = new DutyEmployersJournal();
                    tp.Controls.Add(_dutyEmpJournal);
                    break;
                case "Node14"://Журнали->Заявки
                    _bidJournalCtrl = new BidJoutnalCtrl();
                    tp.Controls.Add(_bidJournalCtrl);
                    break;
                case "Node16"://Журнали->Зміни паролів
                    dg.DataSource = from c in fl.change_password select c;
                    tp.Controls.Add(dg);
                    break;
                case "Node17"://Журнали->Зміни конфігурацій
                    dg.DataSource = from c in fl.change_configuration select c;
                    tp.Controls.Add(dg);
                    break;
                case "Node18"://Журнали->Чергування ТГ
                    _dutyJournalTechGroup = new DutyJournalTechGroup(DataAccess.ConnectString);
                    tp.Controls.Add(_dutyJournalTechGroup);
                    break;
                case "Node19"://Журнали->Договори на обслуговування
                    dg.DataSource = from c in fl.company_contracts_view orderby c.ContractStartDate select c;
                    tp.Controls.Add(dg);
                    break;
                case "Node20"://Журнали->Тимчасово зняті
                    dg.DataSource = from c in fl.rep_temporary_taken_off orderby c.ChangeStatusDate select c;
                    tp.Controls.Add(dg);
                    break;
                case "Node21"://Журнали->Зняті зі спостерігання
                    dg.DataSource = from c in fl.guard_object_journal_view where c.StatusId == 5 select c;
                    tp.Controls.Add(dg);
                    break;
                case "Node35"://Тривоги\Несправності
                    _alertsFaults = new AlertsFaults(DataAccess.ConnectString);
                    tp.Controls.Add(_alertsFaults);
                    break;
                case "Node30"://Довідники->Сповіщення->Категорії сповіщень
                    dg.DataSource = from t0 in fl.s_alarm_category
                                    join t1 in fl.action_set on new { ActionSetId = t0.ActionSetId.Value } equals new { ActionSetId = t1.Id }
                                    select new
                                    {
                                        t0.Id,
                                        t0.Name,
                                        t0.Priority,
                                        Column1 = t1.Name
                                    };
                    tp.Controls.Add(dg);
                    break;
                case "Node31"://Довідники->Сповіщення->Системи пожежної автоматики
                    dg.DataSource = from c in fl.responsibility select c.Name;
                    tp.Controls.Add(dg);
                    break;
                case "Node32"://Довідники->Сповіщення->План дій
                    dg.DataSource = from c in fl.responsibility select c.Name;
                    tp.Controls.Add(dg);
                    break;
                case "Node33"://Довідники->Сповіщення->Сповіщення
                    dg.DataSource = from t0 in fl.alarm
                                    join t1 in fl.s_alarm_category on new { AlarmCategoryId = t0.AlarmCategoryId } equals new { AlarmCategoryId = t1.Id }
                                    select new
                                    {
                                        t0.AlarmCode,
                                        t0.Name,
                                        t0.Description,
                                        Category = t1.Name
                                    };
                    tp.Controls.Add(dg);
                    break;
                case "Node34"://Довідники->Сповіщення->Причини тривог
                    dg.DataSource = from c in fl.alarm_reason select c.Name;
                    tp.Controls.Add(dg);
                    break;
                


                case "Node37"://Черга сповіщень
                    _queueNotifications = new QueueNotifications();
                    tp.Controls.Add(_queueNotifications);
                    break;
                
                //case "Node"://Журнали->
                //    dg.DataSource =
                //    tp.Controls.Add(dg);
                //    break;
            }


            tabControl1.TabPages.Add(tp);
            //select current tab
            tabControl1.SelectedTab = tp;
            //
            if (tp.Controls.Contains(dg))
            {
                dg.Height = tp.Height;
                dg.Width = tp.Width;
                //hide datagridview columns, rename to Ukrainian                     
                HideColumns(dg, e.Node.Name);
            }

        }
        void HideColumns(DataGridView dg, string selectedNodeName)
        {
            switch (selectedNodeName)
            {
                case "Node9"://Журнал несправностей
                    if (_faultJornal == null) return;
                    //Сповіщення
                    //FaultJornal.dataGridView2.Columns[""].HeaderText = "";
                    //FaultJornal.dataGridView2.Columns["Note"].HeaderText = "Текст";
                    //FaultJornal.dataGridView2.Columns["Address"].HeaderText = "Адреса";
                    //FaultJornal.dataGridView2.Columns["AlarmCode"].HeaderText = "Код сповіщення";
                    //FaultJornal.dataGridView2.Columns["AlarmName"].HeaderText = "Сповіщення";
                    //FaultJornal.dataGridView2.Columns["DemandId"].HeaderText = "№ Заявки";
                    //FaultJornal.dataGridView2.Columns[""].HeaderText = "";
                    //FaultJornal.dataGridView2.Columns[""].HeaderText = "";
                    //FaultJornal.dataGridView2.Columns[""].HeaderText = "";
                    //FaultJornal.dataGridView2.Columns[""].HeaderText = "";
                    //FaultJornal.dataGridView2.Columns[""].HeaderText = "";
                    //FaultJornal.dataGridView2.Columns[""].HeaderText = "";
                    //FaultJornal.dataGridView2.Columns[""].HeaderText = "";
                    //FaultJornal.dataGridView2.Columns[""].HeaderText = "";
                    //FaultJornal.dataGridView2.Columns[""].HeaderText = "";
                    //FaultJornal.dataGridView2.Columns[""].HeaderText = "";
                    //FaultJornal.dataGridView2.Columns[""].HeaderText = "";
                    //FaultJornal.dataGridView2.Columns[""].HeaderText = "";
                    //FaultJornal.dataGridView2.Columns[""].HeaderText = "";
                    //FaultJornal.dataGridView2.Columns[""].HeaderText = "";
                    //FaultJornal.dataGridView2.Columns[""].HeaderText = "";
                    //FaultJornal.dataGridView2.Columns[""].HeaderText = "";
                    //FaultJornal.dataGridView2.Columns[""].HeaderText = "";
                    //FaultJornal.dataGridView2.Columns[""].HeaderText = "";
                    //FaultJornal.dataGridView2.Columns[""].HeaderText = "";
                    //FaultJornal.dataGridView2.Columns[""].HeaderText = "";
                    //FaultJornal.dataGridView2.Columns[""].HeaderText = "";

                    break;
                case "Node15"://Журнали->Журнали чергування співробітників

                    break;
                case "Node16"://Журнали->Зміни паролів
                    dg.Columns["id"].Visible = false;
                    dg.Columns["Login"].HeaderText = "Логін";
                    dg.Columns["Date"].HeaderText = "Час зміни пароля";
                    dg.Columns["WhoChangeLogin"].HeaderText = "Хто змінив";
                    dg.Columns["rowguid"].Visible = false;
                    dg.ReadOnly = false;
                    dg.EditMode = DataGridViewEditMode.EditOnEnter;
                    dg.EndEdit();
                    break;
                case "Node17"://Журнали->Зміни конфігурацій
                    dg.Columns["id"].Visible = false;
                    dg.Columns["rowguid"].Visible = false;

                    dg.Columns["Login"].HeaderText = "Логін";
                    dg.Columns["Date"].HeaderText = "Час зміни пароля";
                    dg.Columns["Сonfiguration"].HeaderText = "Конфігурація";
                    break;
                case "Node18"://Журнали->Чергування ТГ

                    _dutyJournalTechGroup.dataGridView1.Columns["DriverPersonnelId"].Visible = false;
                    _dutyJournalTechGroup.dataGridView1.Columns["id"].Visible = false;
                    _dutyJournalTechGroup.dataGridView1.Columns["MainPersonnelId"].Visible = false;
                    _dutyJournalTechGroup.dataGridView1.Columns["TechGroupId"].Visible = false;
                    _dutyJournalTechGroup.dataGridView1.Columns["VehicleId"].Visible = false;
                    _dutyJournalTechGroup.dataGridView1.Columns["DaysElapsed"].Visible = false;

                    _dutyJournalTechGroup.dataGridView1.Columns["DriverPersonnelName"].HeaderText = "Водій";
                    _dutyJournalTechGroup.dataGridView1.Columns["FinishDate"].HeaderText = "Кінець зміни";
                    _dutyJournalTechGroup.dataGridView1.Columns["MainPersonnelName"].HeaderText = "Головний групи";
                    _dutyJournalTechGroup.dataGridView1.Columns["OnDate"].HeaderText = "Дата";
                    _dutyJournalTechGroup.dataGridView1.Columns["StartDate"].HeaderText = "Початок зміни";
                    _dutyJournalTechGroup.dataGridView1.Columns["VehicleName"].HeaderText = "Транспортний засіб";
                    _dutyJournalTechGroup.dataGridView1.Columns["TechGroupName"].HeaderText = "Група";
                    break;
                case "Node19"://Журнали->Договори на обслуговування
                    dg.Columns["ResponsibleCompanyId"].Visible = false;
                    dg.Columns["id"].Visible = false;
                    dg.Columns["GuardObjectId"].Visible = false;
                    dg.Columns["ResponsibilityId"].Visible = false;

                    dg.Columns["CompanyContractNo"].HeaderText = "Договір";
                    dg.Columns["ContractEndDate"].HeaderText = "Закінчення договору";
                    dg.Columns["ContractStartDate"].HeaderText = "Початок договору";
                    dg.Columns["DayLeft"].HeaderText = "Залишилось днів";
                    dg.Columns["EDRPOY"].HeaderText = "Код ЄДРПОУ";
                    dg.Columns["GuardObjectName"].HeaderText = "Об'єкт";
                    dg.Columns["GuardObjectNo"].HeaderText = "Код об'єкту";
                    dg.Columns["Name"].HeaderText = "Обслуговуюча організація";
                    dg.Columns["Note"].HeaderText = "Примітка";
                    dg.Columns["Phone1"].HeaderText = "Телефон";
                    dg.Columns["ResponsibilityName"].HeaderText = "Система пож. автоматики";
                    break;
                case "Node20"://Журнали->Тимчасово зняті
                    dg.Columns["GuardObjectId"].Visible = false;
                    dg.Columns["AddressId"].Visible = false;

                    dg.Columns["ChangeStatusDate"].HeaderText = "Дата зняття";
                    dg.Columns["GuardObjectNo"].HeaderText = "Код об'єкту";
                    dg.Columns["ShortName"].HeaderText = "Об'єкт";
                    dg.Columns["ObjectAddress"].HeaderText = "Адреса";
                    dg.Columns["ChangeReason"].HeaderText = "Причина зняття";
                    dg.Columns["NotifyDate"].HeaderText = "Час повідомлення";
                    dg.Columns["CloseStatusDate"].HeaderText = "Дата постановки";
                    dg.Columns["DemandId"].HeaderText = "Заявка";
                    break;
                case "Node21"://Журнали->Зняті зі спостерігання
                    dg.Columns["id"].Visible = false;
                    dg.Columns["ExecutiveGroupId"].Visible = false;
                    dg.Columns["ExecutiveElectricianId"].Visible = false;
                    dg.Columns["DepartureRegionId"].Visible = false;
                    dg.Columns["CityRegionId"].Visible = false;
                    dg.Columns["ObjectCategory"].Visible = false;
                    dg.Columns["ResId"].Visible = false;
                    dg.Columns["ObjectKindId"].Visible = false;
                    dg.Columns["StatusId"].Visible = false;
                    dg.Columns["AddressId"].Visible = false;

                    dg.Columns["GuardObjectNo"].HeaderText = "Код об'єкту";
                    dg.Columns["StatusName"].HeaderText = "Статус";
                    dg.Columns["Name"].HeaderText = "Назва";
                    dg.Columns["FullName"].HeaderText = "Повна назва";
                    dg.Columns["ObjectAddress"].HeaderText = "Адреса";
                    dg.Columns["Phones"].HeaderText = "Телефони";
                    dg.Columns["ObjectKindName"].HeaderText = "Тип";
                    dg.Columns["ObjectCategoryName"].HeaderText = "Категорія";
                    dg.Columns["ResName"].HeaderText = "РЕМ";
                    dg.Columns["Note"].HeaderText = "Примітка";
                    dg.Columns["ContractNo"].HeaderText = "№ договору";
                    break;
                


            }
        }

        private void tabControl1_MouseDown(object sender, MouseEventArgs e)
        {
            for (int i = 0; i < this.tabControl1.TabPages.Count; i++)
            {
                Rectangle r = tabControl1.GetTabRect(i);
                //Getting the position of the "x" mark.
                Rectangle closeButton = new Rectangle(r.Right - 15, r.Top + 4, 9, 7);
                if (closeButton.Contains(e.Location))
                {
                    this.tabControl1.TabPages.RemoveAt(i);
                    break;
                }
            }
        }

        #region При виборі в treeview Оповіщення
        void SendButton_Click(object sender, EventArgs e)
        {
            //через смс
            if ((_autoDzvCntrl.dataGridView1.Rows[_autoDzvCntrl.dataGridView1.CurrentCellAddress.Y].DataBoundItem as PersonAlert).AlertSms
                && _autoDzvCntrl.smsAndroid.Checked)
                SendSmsViaAndroidPhone(_autoDzvCntrl.phone.Text, _autoDzvCntrl.textBox1.Text);
            //cмс через интернет
            if ((_autoDzvCntrl.dataGridView1.Rows[_autoDzvCntrl.dataGridView1.CurrentCellAddress.Y].DataBoundItem as PersonAlert).AlertSms
                && _autoDzvCntrl.smsInternet.Checked)
                SendSmsViaInet(_autoDzvCntrl.phone.Text, _autoDzvCntrl.textBox1.Text, 0);
        }

        void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (sender is DataGridView)
            {
                _autoDzvCntrl.phone.Text = (_autoDzvCntrl.dataGridView1.Rows[e.RowIndex].DataBoundItem as PersonAlert).cellPhone;
                _autoDzvCntrl.ResponPerson.Text = (_autoDzvCntrl.dataGridView1.Rows[e.RowIndex].DataBoundItem as PersonAlert).RespPersonName;

                

            }
        }

        //при редагуванны рядка в Оповіщені
        void bs_CurrentItemChanged(object sender, EventArgs e)
        {
            if (sender is BindingSource)
            {
                BindingSource bs = (BindingSource)sender;
                if (bs.Current is PersonAlert)
                {
                    PersonAlert pa = (PersonAlert)bs.Current;


                    FireLinqDataContext fl = new FireLinqDataContext(DataAccess.ConnectString);
                    //chech this id in AlerResponPersonOption.Responsible_personID
                    bool idExist = fl.AlerResponPersonOption.Any(c => c.Responsible_personID == pa.id);
                    //существует ли такой Responsible_Person
                    bool idExistResponsPerson = fl.responsible_person.Any(c => c.Id == pa.id);
                    if (idExist && idExistResponsPerson)
                    {
                        AlerResponPersonOption alertPerson = fl.AlerResponPersonOption.Where(c => c.Responsible_personID == pa.id).Single();
                        alertPerson.AlertCellPhone = pa.AlertSms;
                        alertPerson.AlertEmail = pa.AlertEmail;
                        fl.AlerResponPersonOption.InsertOnSubmit(alertPerson);
                        fl.SubmitChanges();
                        
                    }
                    else if (idExistResponsPerson)
                    {
                        AlerResponPersonOption alertPerson = new AlerResponPersonOption();
                        alertPerson.Responsible_personID = pa.id;
                        alertPerson.AlertCellPhone = pa.AlertSms;
                        alertPerson.AlertEmail = pa.AlertEmail;

                        fl.AlerResponPersonOption.InsertOnSubmit(alertPerson);
                        fl.SubmitChanges();
                    }
                }
            }
        } 
        #endregion

        #region Open|close Splitter
        //кнопка для открытия/закрытия splitter
        private void SplitterBtn_Click(object sender, EventArgs e)
        {
            if (splitContainer1.Panel1Collapsed)
            {
                OpenSplitter();
            }
            else
            {
                CloseSplitter();
            }
        }

        private void CloseSplitter()
        {
            splitContainer1.Panel1Collapsed = true;
            SplitterBtn.Image = (Image)Properties.Resources.arrow_open1;
        }

        private void OpenSplitter()
        {
            splitContainer1.Panel1Collapsed = false;
            SplitterBtn.Image = (Image)Properties.Resources.arrow_close;
        } 
        #endregion

        //кнопка у верхньому меню "Налаштування оповіщення"
        private void toolStripMenuItem1_Click_1(object sender, EventArgs e)
        {
            AlertOptionsForm_.ShowDialog();
        }

        // Показати логи
        private void логПрограмиToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LogsForm logs = new LogsForm();
            logs.ShowDialog();
        }

        private void treeView1_AfterSelect(object sender, TreeViewEventArgs e)
        {

        }

        private void statusStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            

   
        }

        private void tabControl1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void editToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Ed ae = new Ed();
            ae.ShowDialog();
        }


        
    }

}
