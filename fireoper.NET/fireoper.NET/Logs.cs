﻿using System;
using System.Windows.Forms;
using fireoper.NET.SharedClasses;

namespace fireoper.NET
{
    /* Форма для показу логів*/
    public partial class LogsForm : Form
    {
        public LogsForm()
        {
            InitializeComponent();
        }

        public void textBoxValue(string text) {
            textBox1.Text += text+Environment.NewLine;
        }

        public void showProgramLogs() {
            textBox1.Text = Logs.getLogs();
        }

        private void CloseButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void ClearButton_Click(object sender, EventArgs e)
        {
            textBox1.Clear();
        }

        private void LogsForm_Load(object sender, EventArgs e)
        {

        }

        
    }
}
