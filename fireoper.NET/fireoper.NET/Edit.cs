﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Data.SqlTypes;
namespace fireoper.NET
{
    public partial class Ed : Form
    {

        SqlConnection con;
        SqlDataAdapter adap;
        DataSet ds;
        SqlCommandBuilder scmb;


        public Ed()
        {
            InitializeComponent();
        }

        private void Ed_Load(object sender, EventArgs e)
        {

            comboBox1.SelectedIndex = 0;
            try {
                con = new SqlConnection();
                con.ConnectionString = (DataAccess.ConnectString);
                con.Open();
                
            }
            catch (Exception ex) {
                MessageBox.Show("Error in " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            
        }

        private void svbtn_Click(object sender, EventArgs e)
        {

            DialogResult dialogResult = MessageBox.Show("Are you sure you want to save changes?", "Save", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
                try
                {
                    scmb = new SqlCommandBuilder(adap);
                    adap.Update(ds, "tb");
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error in " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else if (dialogResult == DialogResult.No)
            {
                SqlDataAdapter ad = adap;
                con = new SqlConnection();
                con.ConnectionString = (DataAccess.ConnectString);
                ds.Reset();
                con.Open();
                adap = ad;
                ds = new System.Data.DataSet();
                adap.Fill(ds, "tb");
                dataGridView1.DataSource = ds.Tables[0];
                
            }
            
            
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

            switch (comboBox1.SelectedIndex)
            {
                case 0:
                    try
                    {
                        con = new SqlConnection();
                        con.ConnectionString = (DataAccess.ConnectString);
                        con.Open();
                        adap = new SqlDataAdapter("select * from responsible_person", con);
                        ds = new System.Data.DataSet();
                        adap.Fill(ds, "tb");
                        dataGridView1.DataSource = ds.Tables[0];
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Error in " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    break;

                case 1:
                    try
                    {
                        con = new SqlConnection();
                        con.ConnectionString = (DataAccess.ConnectString);
                        con.Open();
                        adap = new SqlDataAdapter("select * from addr_area", con);
                        ds = new System.Data.DataSet();
                        adap.Fill(ds, "tb");
                        dataGridView1.DataSource = ds.Tables[0];
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Error in " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    break;
                case 2:
                    try
                    {
                        con = new SqlConnection();
                        con.ConnectionString = (DataAccess.ConnectString);
                        con.Open();
                        adap = new SqlDataAdapter("select * from guard_object", con);
                        ds = new System.Data.DataSet();
                        adap.Fill(ds, "tb");
                        dataGridView1.DataSource = ds.Tables[0];
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Error in " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    break;
                case 3:
                    try
                    {
                        con = new SqlConnection();
                        con.ConnectionString = (DataAccess.ConnectString);
                        con.Open();
                        adap = new SqlDataAdapter("select * from equipment_model", con);
                        ds = new System.Data.DataSet();
                        adap.Fill(ds, "tb");
                        dataGridView1.DataSource = ds.Tables[0];
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Error in " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    break;










            }

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
