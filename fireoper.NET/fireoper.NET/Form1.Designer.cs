﻿namespace fireoper.NET
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.TreeNode treeNode1 = new System.Windows.Forms.TreeNode("Адресна інформація");
            System.Windows.Forms.TreeNode treeNode2 = new System.Windows.Forms.TreeNode("Типи об\'єктів");
            System.Windows.Forms.TreeNode treeNode3 = new System.Windows.Forms.TreeNode("Категорії об\'єктів");
            System.Windows.Forms.TreeNode treeNode4 = new System.Windows.Forms.TreeNode("Об\'єкти", new System.Windows.Forms.TreeNode[] {
            treeNode2,
            treeNode3});
            System.Windows.Forms.TreeNode treeNode5 = new System.Windows.Forms.TreeNode("Комунікатори");
            System.Windows.Forms.TreeNode treeNode6 = new System.Windows.Forms.TreeNode("ППК");
            System.Windows.Forms.TreeNode treeNode7 = new System.Windows.Forms.TreeNode("Кола");
            System.Windows.Forms.TreeNode treeNode8 = new System.Windows.Forms.TreeNode("Сповіщувачі");
            System.Windows.Forms.TreeNode treeNode9 = new System.Windows.Forms.TreeNode("Modbus");
            System.Windows.Forms.TreeNode treeNode10 = new System.Windows.Forms.TreeNode("Апаратура", new System.Windows.Forms.TreeNode[] {
            treeNode5,
            treeNode6,
            treeNode7,
            treeNode8,
            treeNode9});
            System.Windows.Forms.TreeNode treeNode11 = new System.Windows.Forms.TreeNode("Система");
            System.Windows.Forms.TreeNode treeNode12 = new System.Windows.Forms.TreeNode("Персонал");
            System.Windows.Forms.TreeNode treeNode13 = new System.Windows.Forms.TreeNode("Категорії сповіщень");
            System.Windows.Forms.TreeNode treeNode14 = new System.Windows.Forms.TreeNode("Системи пожежної автоматики");
            System.Windows.Forms.TreeNode treeNode15 = new System.Windows.Forms.TreeNode("План дій");
            System.Windows.Forms.TreeNode treeNode16 = new System.Windows.Forms.TreeNode("Сповіщення");
            System.Windows.Forms.TreeNode treeNode17 = new System.Windows.Forms.TreeNode("Причини тривог");
            System.Windows.Forms.TreeNode treeNode18 = new System.Windows.Forms.TreeNode("Сповіщення", new System.Windows.Forms.TreeNode[] {
            treeNode13,
            treeNode14,
            treeNode15,
            treeNode16,
            treeNode17});
            System.Windows.Forms.TreeNode treeNode19 = new System.Windows.Forms.TreeNode("Довідники", new System.Windows.Forms.TreeNode[] {
            treeNode1,
            treeNode4,
            treeNode10,
            treeNode11,
            treeNode12,
            treeNode18});
            System.Windows.Forms.TreeNode treeNode20 = new System.Windows.Forms.TreeNode("Журнал пожеж");
            System.Windows.Forms.TreeNode treeNode21 = new System.Windows.Forms.TreeNode("Журнал несправностей");
            System.Windows.Forms.TreeNode treeNode22 = new System.Windows.Forms.TreeNode("Журнал тривог");
            System.Windows.Forms.TreeNode treeNode23 = new System.Windows.Forms.TreeNode("Сповіщення");
            System.Windows.Forms.TreeNode treeNode24 = new System.Windows.Forms.TreeNode("Об\'єкти");
            System.Windows.Forms.TreeNode treeNode25 = new System.Windows.Forms.TreeNode("Апаратура");
            System.Windows.Forms.TreeNode treeNode26 = new System.Windows.Forms.TreeNode("Заявки");
            System.Windows.Forms.TreeNode treeNode27 = new System.Windows.Forms.TreeNode("Журнал чергування співробітників");
            System.Windows.Forms.TreeNode treeNode28 = new System.Windows.Forms.TreeNode("Журнал зміни паролів");
            System.Windows.Forms.TreeNode treeNode29 = new System.Windows.Forms.TreeNode("Журнал зміни конфігурації");
            System.Windows.Forms.TreeNode treeNode30 = new System.Windows.Forms.TreeNode("Журнал чергування ТГ");
            System.Windows.Forms.TreeNode treeNode31 = new System.Windows.Forms.TreeNode("Договори на обслуговування");
            System.Windows.Forms.TreeNode treeNode32 = new System.Windows.Forms.TreeNode("Тимчасово зняті");
            System.Windows.Forms.TreeNode treeNode33 = new System.Windows.Forms.TreeNode("Зняті зі спостерігання");
            System.Windows.Forms.TreeNode treeNode34 = new System.Windows.Forms.TreeNode("Журнали", new System.Windows.Forms.TreeNode[] {
            treeNode20,
            treeNode21,
            treeNode22,
            treeNode23,
            treeNode24,
            treeNode25,
            treeNode26,
            treeNode27,
            treeNode28,
            treeNode29,
            treeNode30,
            treeNode31,
            treeNode32,
            treeNode33});
            System.Windows.Forms.TreeNode treeNode35 = new System.Windows.Forms.TreeNode("Журнал оповіщень");
            System.Windows.Forms.TreeNode treeNode36 = new System.Windows.Forms.TreeNode("Тривоги/Несправності");
            System.Windows.Forms.TreeNode treeNode37 = new System.Windows.Forms.TreeNode("Черга сповіщень");
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.treeView1 = new System.Windows.Forms.TreeView();
            this.SplitterBtn = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.логПрограмиToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.statusStrip1.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // statusStrip1
            // 
            this.statusStrip1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.statusStrip1.AutoSize = false;
            this.statusStrip1.Dock = System.Windows.Forms.DockStyle.None;
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1});
            this.statusStrip1.Location = new System.Drawing.Point(3, 507);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(917, 22);
            this.statusStrip1.TabIndex = 1;
            this.statusStrip1.Text = "statusStrip1";
            this.statusStrip1.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.statusStrip1_ItemClicked);
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(118, 17);
            this.toolStripStatusLabel1.Text = "toolStripStatusLabel1";
            // 
            // tabControl1
            // 
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(622, 472);
            this.tabControl1.TabIndex = 2;
            this.tabControl1.SelectedIndexChanged += new System.EventHandler(this.tabControl1_SelectedIndexChanged);
            this.tabControl1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.tabControl1_MouseDown);
            // 
            // treeView1
            // 
            this.treeView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.treeView1.Location = new System.Drawing.Point(0, 0);
            this.treeView1.Name = "treeView1";
            treeNode1.Name = "Node1";
            treeNode1.Text = "Адресна інформація";
            treeNode2.Name = "node22";
            treeNode2.Text = "Типи об\'єктів";
            treeNode2.ToolTipText = "Типи об\'єктів";
            treeNode3.Name = "node23";
            treeNode3.Text = "Категорії об\'єктів";
            treeNode3.ToolTipText = "Категорії об\'єктів";
            treeNode4.Name = "Node2";
            treeNode4.Text = "Об\'єкти";
            treeNode5.Name = "node24";
            treeNode5.Text = "Комунікатори";
            treeNode6.Name = "node25";
            treeNode6.Text = "ППК";
            treeNode7.Name = "node26";
            treeNode7.Text = "Кола";
            treeNode8.Name = "node27";
            treeNode8.Text = "Сповіщувачі";
            treeNode9.Name = "node28";
            treeNode9.Text = "Modbus";
            treeNode10.Name = "Node3";
            treeNode10.Text = "Апаратура";
            treeNode11.Name = "Node4";
            treeNode11.Text = "Система";
            treeNode12.Name = "Node5";
            treeNode12.Text = "Персонал";
            treeNode13.Name = "Node30";
            treeNode13.Text = "Категорії сповіщень";
            treeNode14.Name = "Node31";
            treeNode14.Text = "Системи пожежної автоматики";
            treeNode15.Name = "Node32";
            treeNode15.Text = "План дій";
            treeNode16.Name = "Node33";
            treeNode16.Text = "Сповіщення";
            treeNode17.Name = "Node34";
            treeNode17.Text = "Причини тривог";
            treeNode18.Name = "Node6";
            treeNode18.Text = "Сповіщення";
            treeNode19.Name = "Node0";
            treeNode19.Text = "Довідники";
            treeNode20.Name = "Node8";
            treeNode20.Text = "Журнал пожеж";
            treeNode21.Name = "Node9";
            treeNode21.Text = "Журнал несправностей";
            treeNode22.Name = "Node10";
            treeNode22.Text = "Журнал тривог";
            treeNode23.Name = "Node11";
            treeNode23.Text = "Сповіщення";
            treeNode24.Name = "Node12";
            treeNode24.Text = "Об\'єкти";
            treeNode25.Name = "Node13";
            treeNode25.Text = "Апаратура";
            treeNode26.Name = "Node14";
            treeNode26.Text = "Заявки";
            treeNode27.Name = "Node15";
            treeNode27.Text = "Журнал чергування співробітників";
            treeNode28.Name = "Node16";
            treeNode28.Text = "Журнал зміни паролів";
            treeNode29.Name = "Node17";
            treeNode29.Text = "Журнал зміни конфігурації";
            treeNode30.Name = "Node18";
            treeNode30.Text = "Журнал чергування ТГ";
            treeNode31.Name = "Node19";
            treeNode31.Text = "Договори на обслуговування";
            treeNode32.Name = "Node20";
            treeNode32.Text = "Тимчасово зняті";
            treeNode33.Name = "Node21";
            treeNode33.Text = "Зняті зі спостерігання";
            treeNode34.Name = "Node7";
            treeNode34.Text = "Журнали";
            treeNode35.Name = "Node29";
            treeNode35.Text = "Журнал оповіщень";
            treeNode36.Name = "Node35";
            treeNode36.Text = "Тривоги/Несправності";
            treeNode37.Name = "Node37";
            treeNode37.Text = "Черга сповіщень";
            this.treeView1.Nodes.AddRange(new System.Windows.Forms.TreeNode[] {
            treeNode19,
            treeNode34,
            treeNode35,
            treeNode36,
            treeNode37});
            this.treeView1.Size = new System.Drawing.Size(249, 472);
            this.treeView1.TabIndex = 3;
            this.treeView1.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.treeView1_AfterSelect);
            this.treeView1.NodeMouseDoubleClick += new System.Windows.Forms.TreeNodeMouseClickEventHandler(this.treeView1_NodeMouseDoubleClick);
            // 
            // SplitterBtn
            // 
            this.SplitterBtn.Location = new System.Drawing.Point(3, 22);
            this.SplitterBtn.Name = "SplitterBtn";
            this.SplitterBtn.Size = new System.Drawing.Size(36, 54);
            this.SplitterBtn.TabIndex = 5;
            this.SplitterBtn.UseVisualStyleBackColor = true;
            this.SplitterBtn.Click += new System.EventHandler(this.SplitterBtn_Click);
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.panel1.Controls.Add(this.btnUpdate);
            this.panel1.Controls.Add(this.SplitterBtn);
            this.panel1.Location = new System.Drawing.Point(0, 32);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(39, 472);
            this.panel1.TabIndex = 6;
            // 
            // btnUpdate
            // 
            this.btnUpdate.Location = new System.Drawing.Point(3, 82);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(36, 54);
            this.btnUpdate.TabIndex = 6;
            this.btnUpdate.UseVisualStyleBackColor = true;
            this.btnUpdate.Click += new System.EventHandler(this.button1_Click);
            // 
            // splitContainer1
            // 
            this.splitContainer1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainer1.Location = new System.Drawing.Point(45, 32);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.treeView1);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.tabControl1);
            this.splitContainer1.Size = new System.Drawing.Size(875, 472);
            this.splitContainer1.SplitterDistance = 249;
            this.splitContainer1.TabIndex = 7;
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem1,
            this.логПрограмиToolStripMenuItem,
            this.editToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(920, 24);
            this.menuStrip1.TabIndex = 8;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(171, 20);
            this.toolStripMenuItem1.Text = "Налаштування оповіщення";
            this.toolStripMenuItem1.Click += new System.EventHandler(this.toolStripMenuItem1_Click_1);
            // 
            // логПрограмиToolStripMenuItem
            // 
            this.логПрограмиToolStripMenuItem.Name = "логПрограмиToolStripMenuItem";
            this.логПрограмиToolStripMenuItem.Size = new System.Drawing.Size(97, 20);
            this.логПрограмиToolStripMenuItem.Text = "Лог програми";
            this.логПрограмиToolStripMenuItem.Click += new System.EventHandler(this.логПрограмиToolStripMenuItem_Click);
            // 
            // editToolStripMenuItem
            // 
            this.editToolStripMenuItem.Name = "editToolStripMenuItem";
            this.editToolStripMenuItem.Size = new System.Drawing.Size(39, 20);
            this.editToolStripMenuItem.Text = "Edit";
            this.editToolStripMenuItem.Click += new System.EventHandler(this.editToolStripMenuItem_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(920, 529);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.statusStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Operator.NET";
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TreeView treeView1;
        private System.Windows.Forms.Button SplitterBtn;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem логПрограмиToolStripMenuItem;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.ToolStripMenuItem editToolStripMenuItem;
    }
}

