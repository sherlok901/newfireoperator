﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using fireoper.NET.SharedClasses;

namespace fireoper.NET.AutoDozvon
{
    internal class NotifiedAlertManager
    {
        //List<NotifiedAlert> NotifiedAlerts { get; set; }
        //alarm_job_journal[] alerts_;
        FireLinqDataContext fl = new FireLinqDataContext(DataAccess.ConnectString);

        internal NotifiedAlertManager()
        {
            fl.CommandTimeout = Options.LinqTimeout;
            //int length = alerts.Count();
            //for (int i = 0; i < length; i++)
            //{
            //    NotifiedAlert notifiedAlert = new NotifiedAlert();
            //    notifiedAlert.AlertID = alerts[i].Id;
            //    notifiedAlert.GuardObjectShortName = alerts[i].guard_object.ShortName;
            //    notifiedAlert.AlarmCategoryName = alerts[i].s_alarm_category.Name;
            //    //alerts[i].guard_object.guard_object_responsible_persons

            //    List<responsible_person> responPersons = fl.guard_object_responsible_persons.Where(n => n.GuardObjectId.Value == alerts[i].GuardObjectId.Value).Select(n => n.responsible_person).ToList();
            //}
        }

        internal void Notify(bool sendViaEmail, bool sendViaSms)
        {
            if(!sendViaEmail && !sendViaSms) return;
            
            //нові повідомлення
            alarm_job_journal[] alerts = GetNewAlerts();
            if (alerts == null) return;

            int length = alerts.Length;
            for (int i = 0; i < length; i++)
            {
                NotifiedAlert notifiedAlert = new NotifiedAlert();
                notifiedAlert.AlertId = alerts[i].Id;
                notifiedAlert.GuardObjectShortName = alerts[i].guard_object.ShortName;
                notifiedAlert.AlarmCategoryName = alerts[i].s_alarm_category.Name;
                //відповідальні особи
                List<responsible_person> responPersons = fl.guard_object_responsible_persons
                    .Where(n => n.GuardObjectId.Value == alerts[i].GuardObjectId.Value)
                    .Select(n => n.responsible_person).ToList();
                notifiedAlert.ResponPersons = responPersons;

                // відправляєм оповіщення кожній відповідальній особі
                if (responPersons != null)
                {
                    int len2 = responPersons.Count;
                    for (int k = 0; k < len2; k++)
                    {
                        if (sendViaEmail) AlertsSenderManager.SendEmail(notifiedAlert, responPersons[k]);
                        if (sendViaSms) AlertsSenderManager.SendSms(notifiedAlert, responPersons[k]);
                    }
                }
            }
        }

        /// <summary>
        /// Нові повідомлення
        /// </summary>
        /// <returns></returns>
        internal alarm_job_journal[] GetNewAlerts()
        {
            alarm_job_journal[] newAlerts =
            (from a in fl.alarm_job_journal
             join n in fl.NotifiedAlerts on a.Id equals n.AlarmJobJournalID
             where a.ClosedTime == null
             select a).ToArray();
            // Видалити все рядки з NotifiedAlerts (нові повідомлення)
            fl.NotifiedAlerts.DeleteAllOnSubmit(fl.NotifiedAlerts.Select(n => n));
            fl.SubmitChanges();
            return newAlerts;
        }
    }
}
