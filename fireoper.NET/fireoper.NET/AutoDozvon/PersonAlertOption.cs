﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace fireoper.NET
{
    internal class PersonAlertOption
    {
         public List<PersonAlert> list;
         FireLinqDataContext fl;

        public PersonAlertOption(string ConnStr)
        {
            fl = new FireLinqDataContext(ConnStr);

           list =( from per in fl.responsible_person
                                     join opt in fl.AlerResponPersonOption
                                     on per.Id equals opt.Responsible_personID into join1
                                     from f in join1.DefaultIfEmpty()
                                     select new PersonAlert {
                                         id=per.Id,
                                         RespPersonName = per.Name,
                                         email = per.Email,
                                         cellPhone = per.CellPhone,
                                         AlertEmail = (f.AlertEmail == null) ? false : f.AlertEmail,
                                         AlertSms = (f.AlertCellPhone == null) ? false : f.AlertCellPhone                                          
                                     }).ToList();
            
        }

        
    }

    internal class PersonAlert
    {
        //hide id column
        [System.ComponentModel.Browsable(false)]
        public int id { get; set; }
        [System.ComponentModel.ReadOnly(true)]
        public string RespPersonName { get; set; }
        public string email { get; set; }
        public string cellPhone { get; set; }
        public bool AlertEmail { get; set; }
        public bool AlertSms { get; set; }
    }
}
