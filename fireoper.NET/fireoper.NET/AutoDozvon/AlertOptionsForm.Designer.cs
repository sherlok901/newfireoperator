﻿namespace fireoper.NET.AutoDozvon
{
    partial class AlertOptionsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.SMScheckBox2 = new System.Windows.Forms.CheckBox();
            this.EmailcheckBox1 = new System.Windows.Forms.CheckBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.AllAlertsTypecheckBox1 = new System.Windows.Forms.CheckBox();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.CheckColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.PortnumericUpDown1 = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.PassmaskedTextBox1 = new System.Windows.Forms.MaskedTextBox();
            this.SSLcheckBox1 = new System.Windows.Forms.CheckBox();
            this.HosttextBox2 = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.EmailtextBox3 = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.CheckPasscheckBox1 = new System.Windows.Forms.CheckBox();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PortnumericUpDown1)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.SMScheckBox2);
            this.groupBox1.Controls.Add(this.EmailcheckBox1);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBox1.Location = new System.Drawing.Point(6, 26);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(253, 97);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Оповіщення";
            // 
            // SMScheckBox2
            // 
            this.SMScheckBox2.AutoSize = true;
            this.SMScheckBox2.Location = new System.Drawing.Point(18, 59);
            this.SMScheckBox2.Name = "SMScheckBox2";
            this.SMScheckBox2.Size = new System.Drawing.Size(185, 20);
            this.SMScheckBox2.TabIndex = 3;
            this.SMScheckBox2.Text = "SMS (потрібен інтернет)";
            this.SMScheckBox2.UseVisualStyleBackColor = true;
            this.SMScheckBox2.CheckedChanged += new System.EventHandler(this.SMScheckBox2_CheckedChanged);
            // 
            // EmailcheckBox1
            // 
            this.EmailcheckBox1.AutoSize = true;
            this.EmailcheckBox1.Location = new System.Drawing.Point(18, 36);
            this.EmailcheckBox1.Name = "EmailcheckBox1";
            this.EmailcheckBox1.Size = new System.Drawing.Size(209, 20);
            this.EmailcheckBox1.TabIndex = 2;
            this.EmailcheckBox1.Text = "на Email (потрібен інтернет)";
            this.EmailcheckBox1.UseVisualStyleBackColor = true;
            this.EmailcheckBox1.CheckedChanged += new System.EventHandler(this.EmailcheckBox1_CheckedChanged);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.AllAlertsTypecheckBox1);
            this.groupBox2.Controls.Add(this.dataGridView1);
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBox2.Location = new System.Drawing.Point(265, 26);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(286, 215);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Оповіщувати при тривогах:";
            // 
            // AllAlertsTypecheckBox1
            // 
            this.AllAlertsTypecheckBox1.AutoSize = true;
            this.AllAlertsTypecheckBox1.Location = new System.Drawing.Point(32, 36);
            this.AllAlertsTypecheckBox1.Name = "AllAlertsTypecheckBox1";
            this.AllAlertsTypecheckBox1.Size = new System.Drawing.Size(102, 20);
            this.AllAlertsTypecheckBox1.TabIndex = 1;
            this.AllAlertsTypecheckBox1.Text = "Всі тривоги";
            this.AllAlertsTypecheckBox1.UseVisualStyleBackColor = true;
            this.AllAlertsTypecheckBox1.CheckedChanged += new System.EventHandler(this.AllAlertsTypecheckBox1_CheckedChanged);
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.CheckColumn});
            this.dataGridView1.Location = new System.Drawing.Point(32, 79);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowHeadersVisible = false;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(222, 117);
            this.dataGridView1.TabIndex = 0;
            this.dataGridView1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            // 
            // CheckColumn
            // 
            this.CheckColumn.HeaderText = "Вибрати";
            this.CheckColumn.Name = "CheckColumn";
            this.CheckColumn.Width = 75;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(557, 537);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(80, 30);
            this.button1.TabIndex = 4;
            this.button1.Text = "ОК";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(12, 537);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(80, 30);
            this.button2.TabIndex = 5;
            this.button2.Text = "Закрити";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(12, 32);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(625, 479);
            this.tabControl1.TabIndex = 6;
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.SystemColors.Control;
            this.tabPage1.Controls.Add(this.groupBox1);
            this.tabPage1.Controls.Add(this.groupBox2);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(617, 453);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Загальні";
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.SystemColors.Control;
            this.tabPage2.Controls.Add(this.groupBox4);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(617, 453);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Email оповіщення";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.CheckPasscheckBox1);
            this.groupBox4.Controls.Add(this.PortnumericUpDown1);
            this.groupBox4.Controls.Add(this.label4);
            this.groupBox4.Controls.Add(this.PassmaskedTextBox1);
            this.groupBox4.Controls.Add(this.SSLcheckBox1);
            this.groupBox4.Controls.Add(this.HosttextBox2);
            this.groupBox4.Controls.Add(this.label8);
            this.groupBox4.Controls.Add(this.label9);
            this.groupBox4.Controls.Add(this.EmailtextBox3);
            this.groupBox4.Controls.Add(this.label10);
            this.groupBox4.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBox4.Location = new System.Drawing.Point(6, 26);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(419, 231);
            this.groupBox4.TabIndex = 10;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Налаштування SMTP клієнта";
            // 
            // PortnumericUpDown1
            // 
            this.PortnumericUpDown1.Location = new System.Drawing.Point(94, 67);
            this.PortnumericUpDown1.Maximum = new decimal(new int[] {
            68000,
            0,
            0,
            0});
            this.PortnumericUpDown1.Name = "PortnumericUpDown1";
            this.PortnumericUpDown1.Size = new System.Drawing.Size(93, 24);
            this.PortnumericUpDown1.TabIndex = 12;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(24, 170);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(65, 18);
            this.label4.TabIndex = 11;
            this.label4.Text = "Пароль:";
            // 
            // PassmaskedTextBox1
            // 
            this.PassmaskedTextBox1.Location = new System.Drawing.Point(186, 167);
            this.PassmaskedTextBox1.Name = "PassmaskedTextBox1";
            this.PassmaskedTextBox1.PasswordChar = '*';
            this.PassmaskedTextBox1.Size = new System.Drawing.Size(191, 24);
            this.PassmaskedTextBox1.TabIndex = 10;
            this.PassmaskedTextBox1.UseSystemPasswordChar = true;
            // 
            // SSLcheckBox1
            // 
            this.SSLcheckBox1.AutoSize = true;
            this.SSLcheckBox1.Location = new System.Drawing.Point(27, 103);
            this.SSLcheckBox1.Name = "SSLcheckBox1";
            this.SSLcheckBox1.Size = new System.Drawing.Size(259, 22);
            this.SSLcheckBox1.TabIndex = 8;
            this.SSLcheckBox1.Text = "Використовується SSL протокол";
            this.SSLcheckBox1.UseVisualStyleBackColor = true;
            // 
            // HosttextBox2
            // 
            this.HosttextBox2.Location = new System.Drawing.Point(94, 37);
            this.HosttextBox2.Name = "HosttextBox2";
            this.HosttextBox2.Size = new System.Drawing.Size(199, 24);
            this.HosttextBox2.TabIndex = 3;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(24, 66);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(47, 18);
            this.label8.TabIndex = 1;
            this.label8.Text = "Порт:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(24, 140);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(137, 18);
            this.label9.TabIndex = 7;
            this.label9.Text = "Ваша email адреса";
            // 
            // EmailtextBox3
            // 
            this.EmailtextBox3.Location = new System.Drawing.Point(185, 137);
            this.EmailtextBox3.Name = "EmailtextBox3";
            this.EmailtextBox3.Size = new System.Drawing.Size(192, 24);
            this.EmailtextBox3.TabIndex = 6;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(24, 40);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(46, 18);
            this.label10.TabIndex = 4;
            this.label10.Text = "Хост:";
            // 
            // CheckPasscheckBox1
            // 
            this.CheckPasscheckBox1.AutoSize = true;
            this.CheckPasscheckBox1.Location = new System.Drawing.Point(185, 197);
            this.CheckPasscheckBox1.Name = "CheckPasscheckBox1";
            this.CheckPasscheckBox1.Size = new System.Drawing.Size(82, 22);
            this.CheckPasscheckBox1.TabIndex = 13;
            this.CheckPasscheckBox1.Text = "Змінити";
            this.CheckPasscheckBox1.UseVisualStyleBackColor = true;
            this.CheckPasscheckBox1.CheckedChanged += new System.EventHandler(this.CheckPasscheckBox1_CheckedChanged);
            // 
            // AlertOptionsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(652, 593);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Name = "AlertOptionsForm";
            this.Text = "Налаштування оповіщень";
            this.Load += new System.EventHandler(this.AlertOptionsForm_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PortnumericUpDown1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.CheckBox SMScheckBox2;
        private System.Windows.Forms.CheckBox EmailcheckBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.CheckBox AllAlertsTypecheckBox1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.DataGridViewCheckBoxColumn CheckColumn;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.MaskedTextBox PassmaskedTextBox1;
        private System.Windows.Forms.CheckBox SSLcheckBox1;
        private System.Windows.Forms.TextBox HosttextBox2;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox EmailtextBox3;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.NumericUpDown PortnumericUpDown1;
        private System.Windows.Forms.CheckBox CheckPasscheckBox1;
    }
}