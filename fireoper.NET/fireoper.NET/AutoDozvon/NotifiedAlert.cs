﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace fireoper.NET.AutoDozvon
{
    /// <summary>
    /// Клас представляє оповіщення тривоги; 
    /// </summary>
    public class NotifiedAlert
    {

        public int AlertId { get; set; }
        public DateTime ReceivedTime { get; set; }

        public string GuardObjectShortName { get; set; }
        public string AlarmCategoryName { get; set; }
        public string ResponsiblePersonEmail { get; set; }
        public string ResponsiblePersonPhone { get; set; }

        public List<responsible_person> ResponPersons { get; set; }
        /// <summary>
        /// Скільки раз було вже оповіщено про дану тривогу
        /// </summary>
        public int CurrentNumberOfAlerting { get; set; }
    }
}
