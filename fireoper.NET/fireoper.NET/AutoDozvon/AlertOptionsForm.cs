﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
//using System.Threading;
using fireoper.NET.SharedClasses;

namespace fireoper.NET.AutoDozvon
{
    /// <summary>
    /// Форма настроек
    /// </summary>
    public partial class AlertOptionsForm : Form
    {
        AutoDozvonController _AutoDozvonController;
        FireLinqDataContext fl = new FireLinqDataContext(DataAccess.ConnectString);
        

        //properties
        public AutoDozvonController AutoDozvonController { get { return _AutoDozvonController; } }


        public AlertOptionsForm(AutoDozvonController autoDozvonOption)
        {
            InitializeComponent();
            _AutoDozvonController = autoDozvonOption;
            this.StartPosition = FormStartPosition.CenterScreen;

            IQueryable<AlarmCategories> categors = from c in fl.s_alarm_category
                                                   select new AlarmCategories
                                                   {
                                                       id = c.Id,
                                                       AlarmCategoryName = c.Name
                                                   };
            dataGridView1.DataSource = categors;
            
        }       

        //close this form
        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        #region Оповіщення
        //Оповищення
        private void NotifyONradioButton1_CheckedChanged(object sender, EventArgs e)
        {
            _AutoDozvonController.AlertsTurnOn = true;
            EmailcheckBox1.Enabled = true;
            SMScheckBox2.Enabled = true;

            AllAlertsTypecheckBox1.Enabled = true;
            dataGridView1.Enabled = true;
        }

        private void NotifyOFFradioButton2_CheckedChanged(object sender, EventArgs e)
        {
            _AutoDozvonController.AlertsTurnOn = false;
            EmailcheckBox1.Enabled = false;
            SMScheckBox2.Enabled = false;
            
            AllAlertsTypecheckBox1.Enabled = false;
            dataGridView1.Enabled = false;
        } 
        #endregion

        //email
        private void EmailcheckBox1_CheckedChanged(object sender, EventArgs e)
        {
            _AutoDozvonController.SendViaEmail = EmailcheckBox1.Checked;
        }
        //sms
        private void SMScheckBox2_CheckedChanged(object sender, EventArgs e)
        {
            _AutoDozvonController.SendViaSms = SMScheckBox2.Checked;
        }

        //Повторне оповищення
        private void RepeatNotifyONradioBtn_CheckedChanged(object sender, EventArgs e)
        {
            _AutoDozvonController.RepeatNotification = true;
        }

        private void RepeatNotifyOffRadioBtn_CheckedChanged(object sender, EventArgs e)
        {
            _AutoDozvonController.RepeatNotification = false;
        }

        private void SecNUpDown1_ValueChanged(object sender, EventArgs e)
        {
            //setTime();
        }

        private void MinNUpDown2_ValueChanged(object sender, EventArgs e)
        {

        }

        private void saveSelectedCategories()
        {
            foreach (DataGridViewRow row in dataGridView1.Rows)
            {
                DataGridViewCheckBoxCell chk = (DataGridViewCheckBoxCell)row.Cells[0];
                int id = (row.DataBoundItem as AlarmCategories).id;
                if (chk.Value == null) continue;
                else if ((bool)chk.Value)
                {
                    if (_AutoDozvonController.SelectedAlarmCategoryId.Contains(id) == false)
                        _AutoDozvonController.SelectedAlarmCategoryId.Add(id);
                    //FiltersChanged(this, EventArgs.Empty);
                }
                else
                {
                    //delete id from collection
                    if (_AutoDozvonController.SelectedAlarmCategoryId.Contains(id))
                    {
                        _AutoDozvonController.SelectedAlarmCategoryId.Remove(id);
                        //FiltersChanged(this, EventArgs.Empty);
                    }
                }
            }
            //зберегти в бд
            //очистити збережені категорії
            fl.Notified_s_alarm_category.DeleteAllOnSubmit(fl.Notified_s_alarm_category.Select(n=>n));
            fl.SubmitChanges();
            //додати ті що виділив користувач
            foreach (var alarmCategoryId in _AutoDozvonController.SelectedAlarmCategoryId)
            {
                Notified_s_alarm_category category = new Notified_s_alarm_category();
                s_alarm_category alarmCategory = fl.s_alarm_category.FirstOrDefault(n => n.Id.Equals(alarmCategoryId));
                if (alarmCategory != null)
                {
                    category.s_alarm_category = alarmCategory;
                    fl.Notified_s_alarm_category.InsertOnSubmit(category);
                }
            }
            fl.SubmitChanges();
        }
            
        //click
        #region Категории тривог
        /// <summary>
        /// click на строке категории тривоги
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            saveSelectedCategories();
        }
        /// <summary>
        /// Клик на Вибрать все категории
        /// </summary>
        /// <param name="ClichedCheckBox"></param>
        /// <param name="dg"></param>
        private void CheckUncheckAllDataGridViewRows(CheckBox ClichedCheckBox, DataGridView dg)
        {
            //set dg rows
            foreach (DataGridViewRow row in dg.Rows)
            {
                row.Cells[0].Value = ClichedCheckBox.Checked;
            }
        }
        //Клик по checkbox Все категории
        private void AllAlertsTypecheckBox1_CheckedChanged(object sender, EventArgs e)
        {
            //виделить все checkbox в строках
            CheckUncheckAllDataGridViewRows(AllAlertsTypecheckBox1, dataGridView1);
            //подобавлять категории по строках
            dataGridView1_CellContentClick(null, null);
        }
        #endregion

        /// <summary>
        /// Ok button click===========================
        /// сохраняєм все настройки
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click(object sender, EventArgs e)
        {
            //параметры автодозвона
            _AutoDozvonController.SendViaEmail = EmailcheckBox1.Checked;
            _AutoDozvonController.SendViaSms = SMScheckBox2.Checked;

            //параметры smtp клиента
            generalOptions options;
            if (fl.generalOptions.Count() == 0)
            {
                //нет сохраненых настроек оповещаний, считываем
                options = new generalOptions();
                SetGeneralOptions(options);
                //save
                fl.generalOptions.InsertOnSubmit(options);
            }
            else
            {
                options = fl.generalOptions.FirstOrDefault();
                SetGeneralOptions(options);
            }
            Options.GeneralOptions = options;
            fl.SubmitChanges();            
            

            saveSelectedCategories();

            CheckPasscheckBox1.Checked = false;
            PassmaskedTextBox1.Text = null;
            Close();
        }


        #region Загрузка настроект з бд=================

        private void LoadOptions()
        {
            //какие типы оповещений выбраны
            generalOptions options = fl.generalOptions.FirstOrDefault();
            CheckCheckboxes(options);
            LoadEmailOptions(options);
        }

        /// <summary>
        /// Показуэмо параметри email-оповіщення
        /// </summary>
        /// <param name="options"></param>
        private void LoadEmailOptions(generalOptions options)
        {
            if (options == null) return;
            
            EmailcheckBox1.Checked = options.emailAlertsNotification;
            SMScheckBox2.Checked = options.smsAlertsNotification;
            HosttextBox2.Text = options.smtpHost;
            decimal port = 0;
            decimal.TryParse(options.smtpPort, out port);
            PortnumericUpDown1.Value = port;
            if (options.enabledSSL != null) SSLcheckBox1.Checked = options.enabledSSL.Value;
            EmailtextBox3.Text = options.smtpEmailAddress;
            if (!string.IsNullOrEmpty(options.criptedEmailPassword))
            {
                PassmaskedTextBox1.Enabled = false;
                CheckPasscheckBox1.Visible = true;
                //PassmaskedTextBox1.Text = DESencript.DecriptStrin(options.criptedEmailPassword,options);
                //Options.GeneralOptions.criptedEmailPassword
            }
            else CheckPasscheckBox1.Visible = false;
            Options.GeneralOptions = options;


        }

        /// <summary>
        /// Отметить чекбоксы категорий
        /// </summary>
        private void CheckCheckboxes(generalOptions options)
        {
            if(options==null) return;

            EmailcheckBox1.Checked = options.emailAlertsNotification;
            SMScheckBox2.Checked = options.smsAlertsNotification;
            
            List<int> alarmCategIds = fl.Notified_s_alarm_category.Select(n => Convert.ToInt32(n.s_alarm_categoryID)).ToList();
            if(alarmCategIds==null) return;

            int categorsCount = fl.s_alarm_category.Count();
            if (categorsCount==alarmCategIds.Count) AllAlertsTypecheckBox1.Checked = true;
            else AllAlertsTypecheckBox1.Checked = false;
            
            foreach (DataGridViewRow row in dataGridView1.Rows)
            {
                int alarmCategId = (row.DataBoundItem as AlarmCategories).id;
                if (alarmCategIds.Contains(alarmCategId))
                    row.Cells[0].Value = true;
                //if (chk.Value == null) continue;
            }
        } 
        #endregion

        //Установка значення полям настроек
        private void SetGeneralOptions(generalOptions options)
        {
            options.emailAlertsNotification = EmailcheckBox1.Checked;
            options.smsAlertsNotification = SMScheckBox2.Checked;
            options.smtpHost = HosttextBox2.Text;
            options.smtpPort = PortnumericUpDown1.Value.ToString();
            options.enabledSSL = SSLcheckBox1.Checked;
            options.smtpEmailAddress = EmailtextBox3.Text;
            string password = PassmaskedTextBox1.Text;
            if (CheckPasscheckBox1.Checked && !string.IsNullOrEmpty(password)) 
            {
                options.criptedEmailPassword = DESencript.GetCriptedString(password, options);
                //options.criptProvider = DESencript.SerializedTripleDesCryptoServiceProvider;
            }
                
        }

        private void AlertOptionsForm_Load(object sender, EventArgs e)
        {
            LoadOptions();
        }

        /// <summary>
        /// Клик checkbox Змінити пароль
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CheckPasscheckBox1_CheckedChanged(object sender, EventArgs e)
        {
            PassmaskedTextBox1.Enabled = CheckPasscheckBox1.Checked;
        }
    }

    internal class AlarmCategories
    {
        [System.ComponentModel.Browsable(false)]
        public int id { get; set; }
        [System.ComponentModel.ReadOnly(true)]
        [System.ComponentModel.DisplayName("Категорія")]
        public string AlarmCategoryName { get; set; }
    }

}
