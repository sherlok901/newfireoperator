﻿using System;
using System.Net;
using System.Net.Mail;
using fireoper.NET.SharedClasses;
using fireoper.NET.AutoDozvon;

namespace fireoper.NET.AutoDozvon
{
    internal class AlertsSenderManager
    {
        //private static SMSC smsc ;

        internal static void SendEmail(NotifiedAlert notifiedAlert, responsible_person person)
        {
            //test!!
            //MessageBox.Show("Вам email! AlertID=" + notifiedAlert.AlertID + " CurrentNumberOfAlerting=" + notifiedAlert.CurrentNumberOfAlerting);
            //return;


            //SendMail("Тривога :НТЦ Охранные системы", "body", "sherlok901@gmail.com", "smtp.gmail.com", 465, "sherlok901", "BUGAENKO", "igorbugaenko21@gmail.com", "");
            string password = Options.GeneralOptions.criptedEmailPassword;
            if (string.IsNullOrEmpty(password) || Options.GeneralOptions == null)
            {
                if (string.IsNullOrEmpty(password)) Logs.writeToLogWindow("Пустой email пароль");
                if (Options.GeneralOptions == null) Logs.writeToLogWindow("Options.GeneralOptions==null");
                return;
            }

            var smtp = new SmtpClient
            {
                //Host = "smtp.gmail.com",
                Host = Options.GeneralOptions.smtpHost,
                //Port = 587,
                Port = Convert.ToInt32(Options.GeneralOptions.smtpPort),
                EnableSsl = (Options.GeneralOptions.enabledSSL != null)?
                    Options.GeneralOptions.enabledSSL.Value :true,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                //Credentials = new NetworkCredential("sherlok901@gmail.com", "")

                Credentials = new NetworkCredential(Options.GeneralOptions.smtpEmailAddress, Options.GetPassword(password, Options.GeneralOptions))
            };
            MailMessage mail = new MailMessage();
            //mail.From = new MailAddress("sherlok901@gmail.com");
            mail.From = new MailAddress(Options.GeneralOptions.smtpEmailAddress);
            //mail.To.Add("igorbugaenko21@gmail.com");
            if (person.Email != null) mail.To.Add(person.Email);
            else return;

            mail.Subject = "НТЦ Охоронні системи: Увага! " + notifiedAlert.AlarmCategoryName + " на обєкті " + notifiedAlert.GuardObjectShortName;
            mail.Body = "Увага! " + notifiedAlert.AlarmCategoryName + " на обєкті " + notifiedAlert.GuardObjectShortName;
            mail.Body += Environment.NewLine+ @"просп. Леся Курбаса, 2В, Київ, Киевская область, 03148 044 494 3105";
            try
            {
                smtp.Send(mail);

            }
            catch (Exception)
            {
                Logs.writeToLogWindow(DateTime.Now + @" Не вдалось відправити email оповіщення про 
                        тривогу/несправність; id тривоги=" + notifiedAlert.AlertId +
                    "; категорія тривоги: " + notifiedAlert.AlarmCategoryName);
            }

        }

        internal static void SendSms(NotifiedAlert notifiedAlert, responsible_person person)
        {
            string objName = notifiedAlert.GuardObjectShortName;
            if (objName.Length > 43) objName = objName.Substring(0, 43);
            string alarmCategor = notifiedAlert.AlarmCategoryName;//макс 12символів
            //без назви обекту 27символыв; на обект залишается 70-27=43
            string text = "НТЦ:на обекте " + objName + " " + alarmCategor;
            SMSC smsc =new SMSC();
            string[] result = smsc.send_sms(person.CellPhone, text, 0);

            /*
                     * Результат функції треба аналізувати так:
                    if (result.Length == 2)
                    {
                        //error occured
                        "ID повід. " це result[0];
                        "Код помилки " це result[1];
                    }
                    else
                    {
                        "Вартість " це result[2];
                        "Баланс " це result[3];
                    }*/
        }

        internal static void SendMail2()
        {


            try
            {

                SmtpClient mySmtpClient = new SmtpClient("my.smtp.exampleserver.net");

                // set smtp-client with basicAuthentication
                mySmtpClient.UseDefaultCredentials = false;
                NetworkCredential basicAuthenticationInfo = new
                   System.Net.NetworkCredential("username", "password");
                mySmtpClient.Credentials = basicAuthenticationInfo;

                // add from,to mailaddresses
                MailAddress from = new MailAddress("test@example.com", "TestFromName");
                MailAddress to = new MailAddress("igorbugaenko21@gmail.com", "TestToName");
                MailMessage myMail = new MailMessage(from, to);

                // add ReplyTo
                MailAddress replyto = new MailAddress("reply@example.com");
                myMail.ReplyTo = replyto;

                // set subject and encoding
                myMail.Subject = "Test message";
                myMail.SubjectEncoding = System.Text.Encoding.UTF8;

                // set body-message and encoding
                myMail.Body = "<b>Test Mail</b><br>using <b>HTML</b>.";
                myMail.BodyEncoding = System.Text.Encoding.UTF8;
                // text or html
                myMail.IsBodyHtml = true;

                mySmtpClient.Send(myMail);
            }

            catch (SmtpException ex)
            {
                throw new ApplicationException
                  ("SmtpException has occured: " + ex.Message);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
