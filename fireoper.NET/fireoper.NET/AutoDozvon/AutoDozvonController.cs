﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using System.Windows.Forms;
//using System.Timers;
using System.Threading;

namespace fireoper.NET.AutoDozvon
{
    public class AutoDozvonController
    {
        // Налаштування автодозвону
        internal bool AlertsTurnOn { get; set; }
        internal bool SendViaSms { get; set; }
        internal bool SendViaEmail { get; set; }
        internal TimeSpan NotificationInterval { get; set; }
        internal bool RepeatNotification { get; set; }
        internal int CountRepeatNotification { get; set; }

        /// <summary>
        /// Інфа для автодозвону
        /// </summary>
        public List<NotifiedAlert> notifiedAlertList { get { return _notifiedAlertsList; } }
        public List<NotifiedAlert> _notifiedAlertsList = new List<NotifiedAlert>(0);

        internal Dictionary<Timer, NotifiedAlert> timers = new Dictionary<Timer, NotifiedAlert>(0);
       
        internal List<int> SelectedAlarmCategoryId = new List<int>(0);
        FireLinqDataContext fl = new FireLinqDataContext(DataAccess.ConnectString);

        public event EventHandler SmsNotify;
        public event EventHandler EmailNotify;

        public AutoDozvonController()
        {

        }

        //public void notify()
        //{
        //    //если автодозвон откл
        //    if (!AlertsTurnOn) return;
        //    //нові повідомлення
        //    alarm_job_journal[] alerts = getNewAlerts(); 
        //    if (alerts == null) return;

        //    int length = alerts.Count();
        //    for (int i = 0; i < length; i++)
        //    {
        //        NotifiedAlert notifiedAlert = new NotifiedAlert();
        //        notifiedAlert.AlertID = alerts[i].Id;
        //        notifiedAlert.GuardObjectShortName = alerts[i].guard_object.ShortName;
        //        notifiedAlert.AlarmCategoryName = alerts[i].s_alarm_category.Name;
        //        notifiedAlert.ResponsiblePersonEmail = fl.responsible_person.Where(n => n.OwnerGuardObjectId.Equals(alerts[i].GuardObjectId.Value)).Select(n => n.Email).FirstOrDefault();
        //        notifiedAlert.ResponsiblePersonPhone = fl.responsible_person.Where(n => n.OwnerGuardObjectId.Equals(alerts[i].GuardObjectId.Value)).Select(n => n.CellPhone).FirstOrDefault();
        //        _notifiedAlertsList.Add(notifiedAlert);
        //        // запускаем події, щоб запустити відправку оповіщень
        //        if (SendViaEmail) EmailNotify(notifiedAlert, EventArgs.Empty);
        //        if (SendViaSms) SmsNotify(notifiedAlert, EventArgs.Empty);

        //        // Якщо повторний автодозвон відключений
        //        if (!RepeatNotification) continue;

        //        Timer timer = new Timer();
                
                
        //        timer.Elapsed += new ElapsedEventHandler(timer_Elapsed);
        //        timer.Interval = NotificationInterval.TotalMilliseconds;
        //        timer.AutoReset = false;
        //        timer.Start();
        //        timers[timer] = notifiedAlert;
        //    }
        //}

        //// Робимо повторне оповіщення
        //void timer_Elapsed(object sender, ElapsedEventArgs e)
        //{
            
        //    Timer t = (Timer)sender;
        //    NotifiedAlert notifiedAlert = timers[t];
        //    //if (!_notifiedAlertsList.Contains(timers[t])) _notifiedAlertsList.Add(timers[t]);

        //    // Більше не повторювати оповіщення
        //    if (!RepeatNotification || notifiedAlert.CurrentNumberOfAlerting >= CountRepeatNotification) 
        //    {
        //        timers.Remove(t);
        //        t.Stop();
        //        t.Close();
                
        //        return; 
        //    }

        //    // Повторне оповіщення
        //    notifiedAlert.CurrentNumberOfAlerting++;
        //    if (SendViaEmail) EmailNotify(notifiedAlert, EventArgs.Empty);
        //    if (SendViaSms) SmsNotify(notifiedAlert, EventArgs.Empty);

            
        //    t.Interval = NotificationInterval.TotalMilliseconds;
        //    t.Start();
        //}

        internal void LoadOptionsFromFile()
        {

        }
        internal void SaveOptionsToFile() { }
        internal void SaveDefaultOptionsToFile() { }
    }
}
