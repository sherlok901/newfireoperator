﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;

namespace fireoper.NET
{
    public partial class ResultForm : Form
    {        
        FireLinqDataContext fl = new FireLinqDataContext(DataAccess.ConnectString);
        List<Guid> AlarmJobJournalGuid_;
        private List<EditedAlertsByUser> lockedAlerts=new List<EditedAlertsByUser>();

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="alarmJobJournalGuid">Виділені тривоги;</param>
        public ResultForm(List<Guid> alarmJobJournalGuid)
        {
            InitializeComponent();
            AlarmJobJournalGuid_ = alarmJobJournalGuid;
            //showing one column in combobox
            ActionComboBox1.DataSource = fl.s_alarm_action.Where(c=>c.Name.Contains("закрит")).Select(c => c != null ? new SAlarmActionTableBean
            {
                Id = c.Id,
                AlarmName = c.Name
            } : null);
            ActionComboBox1.ValueMember = "alarmName";
            
            //залочити повідомлення про тривогу за користувачем
            alarmJobJournalGuid.ForEach(CheckLock);
            fl.SubmitChanges();
        }

        /// <summary>
        /// Виконуємо для кожної тривоги, яку вибрав користувач щоб закрити
        /// </summary>
        /// <param name="guidAlarmJobJournal"></param>
        private void CheckLock(Guid guidAlarmJobJournal)
        {
            // повідомлення
            alarm_job_journal alarmJobJournal = fl.alarm_job_journal.FirstOrDefault(n => n.rowguid.Equals(guidAlarmJobJournal));
            
            if (alarmJobJournal == null) return;

            EditedAlertsByUser editedAlertsByUser = fl.EditedAlertsByUser.FirstOrDefault(r => r.AlarmJobJournalID.Equals(alarmJobJournal.Id));
            // дана тривога не залочена, залочити її
            if (editedAlertsByUser == null)
            {
                p_getSYSUser pGetSysUser = fl.p_getSYSUser.Take(1).FirstOrDefault();
                if (pGetSysUser != null)
                {
                    EditedAlertsByUser lockedAlert = new EditedAlertsByUser
                    {
                        SystemUser = pGetSysUser.login, //DataAccess.UserLogin.ToLower(),
                        alarm_job_journal = alarmJobJournal
                    };
                    lockedAlerts.Add(lockedAlert);
                    fl.EditedAlertsByUser.InsertOnSubmit(lockedAlert);
                }
            }
        }
     
        // закрити повідомлення
        private void button1_Click(object sender, EventArgs e)
        {
            //З циклу нічого не виносити, тому що InsertOnSubmit заносить в БД унікальні об'єкти
            //І не може запихнути один і той же об'єкт двічі. Нажаль.
            foreach (var a in AlarmJobJournalGuid_)
            {
                //saving action for current alert
                alarm_action_journal alarmActionJournal = new alarm_action_journal();

                //get login id
                int personnelId = fl.personnel.Where(c => c.Login.Equals(DataAccess.UserLogin)).Select(c => c.Id).FirstOrDefault();
                alarmActionJournal.PersonnelId = personnelId;
                SAlarmActionTableBean sAlarmActionTableBean = ActionComboBox1.SelectedItem as SAlarmActionTableBean;
                if (sAlarmActionTableBean != null)
                alarmActionJournal.SysActionId =  (byte)sAlarmActionTableBean.Id;
                alarmActionJournal.DTStamp = DateTime.Now;
                alarmActionJournal.Note = textBox1.Text;
                //alarmActionJournal.AddresseeId = int.Parse(textBox2.Text);
                //alarmActionJournal.PersonnelId = int.Parse(textBox3.Text);
                alarmActionJournal.msrepl_tran_version = Guid.NewGuid();
                alarmActionJournal.rowguid = Guid.NewGuid();
                alarmActionJournal.AlarmJobJournalGuid = a;
                
                fl.alarm_action_journal.InsertOnSubmit(alarmActionJournal);

                //closing current alert
                alarm_job_journal alarmJobJournal = fl.alarm_job_journal.FirstOrDefault(c => c.rowguid.Equals(a));
                if (alarmJobJournal != null) alarmJobJournal.ClosedTime = DateTime.Now;
                try{ fl.SubmitChanges();}catch(Exception ex){}
            }
            // розлочити тривоги
            foreach (var alert in lockedAlerts)
            {
                fl.EditedAlertsByUser.DeleteOnSubmit(alert);
            }
            //save all changes 
            //fl.SubmitChanges();
            Close();
        }

        /// <summary>
        /// Клік по кнопці Відміна
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonCancel_Click(object sender, EventArgs e)
        {
            Close();
        }


        internal class SAlarmActionTableBean
        {
            [Browsable(false)]
            public int Id { get; set; }
            public string AlarmName { get; set; }
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void ResultForm_Load(object sender, EventArgs e)
        {

        }
    }
}
