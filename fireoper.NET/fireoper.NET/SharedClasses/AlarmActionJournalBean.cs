﻿using System;

namespace fireoper.NET.SharedClasses
{
    internal class AlarmActionJournalBean
    {
        [System.ComponentModel.Browsable(false)]
        public int Id { get; set; }

        [System.ComponentModel.DisplayName("Час виконання дії")]
        [System.ComponentModel.ReadOnly(true)]
        public DateTime DTStamp { get; set; }

        [System.ComponentModel.DisplayName("Дія")]
        [System.ComponentModel.ReadOnly(true)]
        public string ActionName { get; set; }

        [System.ComponentModel.DisplayName("Адреса")]
        [System.ComponentModel.ReadOnly(true)]
        public string AdresseeName { get; set; }

        [System.ComponentModel.DisplayName("Результат")]
        [System.ComponentModel.ReadOnly(true)]
        public string AlarmActionResultName { get; set; }

        [System.ComponentModel.DisplayName("Оператор")]
        [System.ComponentModel.ReadOnly(true)]
        public string PersonnelName { get; set; }

    }
}
