﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace fireoper.NET.SharedClasses
{
    /* Даний клас зберігає логи в оперативці пк; їх можна продивитись клікнувши "Логи програми"*/
    public static class Logs
    {
        readonly static int logCapasity = 1000;
        private static StringBuilder logs = new StringBuilder(logCapasity);
        
        public static void writeToLogWindow(string logText) 
        {
            if (logText.Length + logs.EnsureCapacity(0) > logCapasity)
                logs.Clear();
            logs.Append(logText);
        }

        public static string getLogs() { return logs.ToString(); }
    }
}
