﻿using System;

namespace fireoper.NET.SharedClasses
{
    internal class AlertsJournalBean
    {
        [System.ComponentModel.Browsable(false)]
        public int Id { get; set; }

        [System.ComponentModel.DisplayName("Час надходження")]
        [System.ComponentModel.ReadOnly(true)]
        public string ReceivedTime { get; set; }

        [System.ComponentModel.DisplayName("Час закриття")]
        [System.ComponentModel.ReadOnly(true)]
        public string ClosedTime { get; set; }

        [System.ComponentModel.DisplayName("Код об'єкту")]
        [System.ComponentModel.ReadOnly(true)]
        public int? ObjectNumber { get; set; }

        [System.ComponentModel.DisplayName("Об'єкт")]
        [System.ComponentModel.ReadOnly(true)]
        public string ObjectName { get; set; }

        [System.ComponentModel.DisplayName("Приміщення")]
        [System.ComponentModel.ReadOnly(true)]
        public string PremisesName { get; set; }

        [System.ComponentModel.DisplayName("Адреса")]
        [System.ComponentModel.ReadOnly(true)]
        public string Address { get; set; }

        [System.ComponentModel.DisplayName("Причина")]
        [System.ComponentModel.ReadOnly(true)]
        public string ReasonName { get; set; }

        [System.ComponentModel.DisplayName("Категорія")]
        [System.ComponentModel.ReadOnly(true)]
        public string AlarmCategoryName { get; set; }

        [System.ComponentModel.Browsable(false)]
        public Guid RowGuid { get; set; }
    }
}
