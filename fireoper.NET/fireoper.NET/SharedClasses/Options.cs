﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace fireoper.NET.SharedClasses
{
    /// <summary>
    /// Класс создан для хранения generalOptions чтоб кажным разом на выбирать данные з бд
    /// </summary>
    internal static class Options
    {
        /// <summary>
        /// Храним generalOptions
        /// </summary>
        internal static generalOptions GeneralOptions
        {
            get;
            set;
        }

        internal static string GetPassword(string criptedPassword, generalOptions options)
        {
            return DESencript.DecriptStrin(criptedPassword,options);
        }

        internal static int LinqTimeout { get; set; }
    }
}
