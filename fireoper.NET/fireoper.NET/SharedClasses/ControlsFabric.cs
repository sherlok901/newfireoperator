﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace fireoper.NET.SharedClasses
{
    internal static class ControlsFabric
    {
        internal static DataGridView getNewDataGridView()
        {
            DataGridView dg = new DataGridView();
            //настройки datagridview
            dg.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dg.AllowUserToAddRows = false;            
            dg.Dock = DockStyle.Fill;
            return dg;
        }
    }
}
