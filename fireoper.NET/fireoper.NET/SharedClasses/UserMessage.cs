﻿using System.Windows;

namespace fireoper.NET.SharedClasses
{
    public static class UserMessage
    {
        /// <summary>
        ///     Show message to user
        /// </summary>
        public static void ShowMessage(string message)
        {
            MessageBox.Show(message);
        }
    }
}