﻿using System;
using System.IO;
using System.Runtime.Remoting.Metadata.W3cXsd2001;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Security.Cryptography;

namespace fireoper.NET.SharedClasses
{
    /// <summary>
    /// Используется для шифрования/розшифрования строки алгоритмом DES
    /// </summary>
    internal class DESencript
    {
        //fields
        //private static TripleDESCryptoServiceProvider myTripleDES_;
        //private static byte[] serializedTripleDESCryptoServiceProvider_;
        
        ////proterties
        //public static byte[] SerializedTripleDesCryptoServiceProvider
        //{
        //    get { return serializedTripleDESCryptoServiceProvider_; }
        //}

        //methods
        /// <summary>
        /// Зашифровать строку; сериализация криптопровайдера
        /// </summary>
        /// <param name="originalString"></param>
        /// <returns></returns>
        public static string GetCriptedString(string originalString, generalOptions options)
        {
            using (TripleDESCryptoServiceProvider myTripleDES_ = new TripleDESCryptoServiceProvider())
            {
                byte[] encrypted = EncryptStringToBytes(originalString, myTripleDES_.Key, myTripleDES_.IV);
                //Serialize(myTripleDES_, options);
                options.keys = myTripleDES_.Key;
                options.vi = myTripleDES_.IV;
                return GetBytesToString(encrypted);
            }
        }

        /// <summary>
        /// Розшифровать строку используя сериализованый криптопровайдер
        /// </summary>
        /// <param name="criptedStr"></param>
        /// <returns></returns>
        public static string DecriptStrin(string criptedStr,generalOptions options)
        {
            byte[] criptedStrBytes = GetStringToBytes(criptedStr);
            return DecryptStringFromBytes(criptedStrBytes,options.keys.ToArray(), options.vi.ToArray());
        }

        //private static void Serialize(TripleDESCryptoServiceProvider myTripleDES, generalOptions options)
        //{
        //    options.keys = myTripleDES.Key;
        //    options.vi = myTripleDES.IV;
        //}

        public static byte[] GetStringToBytes(string value)
        {
            SoapHexBinary shb = SoapHexBinary.Parse(value);
            return shb.Value;
        }

        public static string GetBytesToString(byte[] value)
        {
            SoapHexBinary shb = new SoapHexBinary(value);
            return shb.ToString();
        }

        /// <summary>
        /// Зашифрувати
        /// </summary>
        /// <param name="plainText"></param>
        /// <param name="Key"></param>
        /// <param name="IV"></param>
        /// <returns></returns>
        public static byte[] EncryptStringToBytes(string plainText, byte[] Key, byte[] IV)
        {
            // Check arguments.
            if (plainText == null || plainText.Length <= 0)
                throw new ArgumentNullException("plainText");
            if (Key == null || Key.Length <= 0)
                throw new ArgumentNullException("Key");
            if (IV == null || IV.Length <= 0)
                throw new ArgumentNullException("Key");
            byte[] encrypted;
            // Create an TripleDESCryptoServiceProvider object
            // with the specified key and IV.
            using (TripleDESCryptoServiceProvider tdsAlg = new TripleDESCryptoServiceProvider())
            {
                tdsAlg.Key = Key;
                tdsAlg.IV = IV;

                // Create a decrytor to perform the stream transform.
                ICryptoTransform encryptor = tdsAlg.CreateEncryptor(tdsAlg.Key, tdsAlg.IV);

                // Create the streams used for encryption.
                using (MemoryStream msEncrypt = new MemoryStream())
                {
                    using (CryptoStream csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
                    {
                        using (StreamWriter swEncrypt = new StreamWriter(csEncrypt))
                        {

                            //Write all data to the stream.
                            swEncrypt.Write(plainText);
                        }
                        encrypted = msEncrypt.ToArray();
                    }
                }
            }


            // Return the encrypted bytes from the memory stream.
            return encrypted;

        }

        /// <summary>
        /// Розшифрувати
        /// </summary>
        /// <param name="cipherText"></param>
        /// <param name="Key"></param>
        /// <param name="IV"></param>
        /// <returns></returns>
        public static string DecryptStringFromBytes(byte[] cipherText, byte[] Key, byte[] IV)
        {
            // Check arguments.
            if (cipherText == null || cipherText.Length <= 0)
                throw new ArgumentNullException("cipherText");
            if (Key == null || Key.Length <= 0)
                throw new ArgumentNullException("Key");
            if (IV == null || IV.Length <= 0)
                throw new ArgumentNullException("Key");

            // Declare the string used to hold
            // the decrypted text.
            string plaintext = null;

            // Create an TripleDESCryptoServiceProvider object
            // with the specified key and IV.
            using (TripleDESCryptoServiceProvider tdsAlg = new TripleDESCryptoServiceProvider())
            {
                tdsAlg.Key = Key;
                tdsAlg.IV = IV;

                // Create a decrytor to perform the stream transform.
                ICryptoTransform decryptor = tdsAlg.CreateDecryptor(tdsAlg.Key, tdsAlg.IV);

                // Create the streams used for decryption.
                using (MemoryStream msDecrypt = new MemoryStream(cipherText))
                {
                    using (CryptoStream csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
                    {
                        using (StreamReader srDecrypt = new StreamReader(csDecrypt))
                        {

                            // Read the decrypted bytes from the decrypting stream
                            // and place them in a string.
                            plaintext = srDecrypt.ReadToEnd();
                        }
                    }
                }

            }

            return plaintext;

        }

        //private byte[] GetBytes(string str)
        //{
        //    byte[] bytes = new byte[str.Length * sizeof(char)];
        //    System.Buffer.BlockCopy(str.ToCharArray(), 0, bytes, 0, bytes.Length);
        //    return bytes;
        //}
        //private static string GetString(byte[] bytes)
        //{
        //  char[] chars = new char[bytes.Length / sizeof(char)];
        //  System.Buffer.BlockCopy(bytes, 0, chars, 0, bytes.Length);
        //  return new string(chars);
        //}
    }
}


