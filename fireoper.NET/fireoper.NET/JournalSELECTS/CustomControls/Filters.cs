﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using System.Configuration;

namespace fireoper.NET.JournalSELECTS.CustomControls
{
    public partial class Filters : UserControl
    {
        FireLinqDataContext fl ;
        internal List<int> SelectedGuardObjects=new List<int>(0);
        internal DateTime AlertReceivingTimeFrom;
        internal DateTime AlertReceivingTimeTo;
        internal List<int> SelectedAlarmCategoryId=new List<int>(0);
        internal bool ClosedAlerts { get; private set; }
        internal bool NonClosedAlerts { get; private set; }
        public event EventHandler FiltersChanged;


        public Filters()
        {
            InitializeComponent();
            //var connection = ConfigurationManager.ConnectionStrings["fireoper.NET.Properties.Settings.IgorfireConnectionString1"].ConnectionString;
            fl = new FireLinqDataContext(DataAccess.ConnectString);
            
            Anchor = AnchorStyles.Left | AnchorStyles.Right;
             
            //init datetime to sql server min datetime            
            AlertReceivingTimeFrom = dateTimePicker1.MinDate;
            AlertReceivingTimeTo = dateTimePicker2.MaxDate;
            
            //select guard objects
            IQueryable<GuardObjsTable> objs2=from c in fl.guard_object select new GuardObjsTable
            {                   
                id=c.Id,
                GuardObjShortName=c.ShortName
            };
            dataGridView1GuardObj.DataSource = objs2;
            

            //select alarm category
            IQueryable<AlarmCategories> categors = from c in fl.s_alarm_category
                                                   select new AlarmCategories
                                                       {
                                                           id=c.Id,
                                                           AlarmCategoryName=c.Name
                                                       };
            dataGridView2AlarmCateg.DataSource = categors;
            //set datetimepicker format
            dateTimePicker1.CustomFormat = @"dd.MM.yyyy HH:mm:ss";
            dateTimePicker2.CustomFormat = @"dd.MM.yyyy HH:mm:ss";

            //init events            
            dateTimePicker1.ValueChanged += new EventHandler(dateTimePicker_ValueChanged);
            dateTimePicker2.ValueChanged += new EventHandler(dateTimePicker_ValueChanged);
        }

        void dateTimePicker_ValueChanged(object sender, EventArgs e)
        {
            //if filtering by time user checked
            if (TurnOnOffDataTimePicker.Checked)
            {
                AlertReceivingTimeFrom = dateTimePicker1.Value;
                AlertReceivingTimeTo = dateTimePicker2.Value;
                //trigering event
                FiltersChanged(this, EventArgs.Empty);
            }
        }


        #region Collapse\Expend form
        private void button1_Click(object sender, EventArgs e)
        {
            if (panel1.Visible)
            {
                panel1.Visible = false;
                FilterBtn.Image = (Image)Properties.Resources.filter_btn_open;
            }
            else
            {
                panel1.Visible = true;
                FilterBtn.Image = (Image)Properties.Resources.filter_btn_close;
            }
        } 
        #endregion

        //check/uncheck all guard objects
        private void SelectAllcheckBox1_CheckedChanged(object sender, EventArgs e)
        {            
            CheckUncheckAllDataGridViewRows(SelectAllcheckBox1,dataGridView1GuardObj);
            GetSelectedGuardObjId();
            FiltersChanged(this, EventArgs.Empty);
        }
        
        //record selected guard objects id to collection
        private void GetSelectedGuardObjId()
        {
            SelectedGuardObjects.Clear();
            //record selected guard objects id to collection
            foreach (DataGridViewRow row in dataGridView1GuardObj.Rows)
            {
                DataGridViewCheckBoxCell chk = (DataGridViewCheckBoxCell)row.Cells[0];
                if (chk.Value == null) continue;
                //else if ((CheckState)chk.Value == CheckState.Checked)
                else if ((bool)chk.Value)
                {
                    //represent row as "GuardObjsTable" class
                    int id = (row.DataBoundItem as GuardObjsTable).id;
                    SelectedGuardObjects.Add(id);
                }
            }
        }

        //check/uncheck rows in datagridview
        private void CheckUncheckAllDataGridViewRows(CheckBox ClichedCheckBox, DataGridView dg)
        {           
            //set dg rows
            foreach (DataGridViewRow row in dg.Rows)
            {
                row.Cells[0].Value = ClichedCheckBox.Checked;           
            }
        }

        //alarm categories selected changed
        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            //check/uncheck all items
            CheckUncheckAllDataGridViewRows(checkBox1, dataGridView2AlarmCateg);
            GetSelectedAlarmCategoryObjects();
            FiltersChanged(this, EventArgs.Empty);
        }

        //record selected alarm category id to collection
        private void GetSelectedAlarmCategoryObjects()
        {
            SelectedAlarmCategoryId.Clear();

            foreach (DataGridViewRow row in dataGridView2AlarmCateg.Rows)
            {
                DataGridViewCheckBoxCell chk = (DataGridViewCheckBoxCell)row.Cells[0];
                if (chk.Value == null) continue;                
                else if ((bool)chk.Value)
                {
                    //represent row as "GuardObjsTable" class
                    int id = (row.DataBoundItem as AlarmCategories).id;
                    SelectedAlarmCategoryId.Add(id);
                }
            }
        }


        #region dataGridView1 guard obj click
        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            foreach (DataGridViewRow row in dataGridView1GuardObj.Rows)
            {
                DataGridViewCheckBoxCell chk = (DataGridViewCheckBoxCell)row.Cells[0];
                int id = (row.DataBoundItem as GuardObjsTable).id;
                if (chk.Value == null) continue;                
                else if ((bool)chk.Value)
                {
                    //selected guard object add to collection
                    SelectedGuardObjects.Add(id);
                    FiltersChanged(this, EventArgs.Empty);
                }
                else
                {
                    //delete id from collection
                    if (SelectedGuardObjects.Contains(id))
                    {
                        SelectedGuardObjects.Remove(id);
                        FiltersChanged(this, EventArgs.Empty);
                    }
                }
            }
        }

        //commit the datagridview edit mode        
        //This event typically occurs when a cell has been edited but the change has not been committed 
        //to the data cache, or when an edit operation is canceled.
        private void dataGridView1_CurrentCellDirtyStateChanged(object sender, EventArgs e)
        {
            if (dataGridView1GuardObj.IsCurrentCellDirty)
            {
                dataGridView1GuardObj.CommitEdit(DataGridViewDataErrorContexts.Commit);
            }
        } 
        #endregion

        internal class GuardObjsTable
        {
            [System.ComponentModel.Browsable(false)]
            public int id { get; set; }
            [System.ComponentModel.ReadOnly(true)]
            [System.ComponentModel.DisplayName("Обєкт")]
            public string GuardObjShortName { get; set; }
        }
        internal class AlarmCategories
        {
            [System.ComponentModel.Browsable(false)]
            public int id { get; set; }
            [System.ComponentModel.ReadOnly(true)]
            [System.ComponentModel.DisplayName("Категорія")]
            public string AlarmCategoryName { get; set; }
        }

        private void dataGridView2AlarmCateg_CurrentCellDirtyStateChanged(object sender, EventArgs e)
        {
            if (dataGridView2AlarmCateg.IsCurrentCellDirty)
            {
                dataGridView2AlarmCateg.CommitEdit(DataGridViewDataErrorContexts.Commit);
            }
        }

        private void dataGridView2AlarmCateg_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            foreach (DataGridViewRow row in dataGridView2AlarmCateg.Rows)
            {
                DataGridViewCheckBoxCell chk = (DataGridViewCheckBoxCell)row.Cells[0];
                int id = (row.DataBoundItem as AlarmCategories).id;
                if (chk.Value == null) continue;               
                else if ((bool)chk.Value)
                {
                    //selected guard object add to collection
                    SelectedAlarmCategoryId.Add(id);
                    FiltersChanged(this, EventArgs.Empty);
                }
                else
                {
                    //delete id from collection
                    if (SelectedAlarmCategoryId.Contains(id))
                    {
                        SelectedAlarmCategoryId.Remove(id);
                        FiltersChanged(this, EventArgs.Empty);
                    }
                }
            }
        }

        private void TurnOnOffDataTimePicker_CheckedChanged(object sender, EventArgs e)
        {
            if (TurnOnOffDataTimePicker.Checked)
            {
                AlertReceivingTimeFrom = dateTimePicker1.Value;
                AlertReceivingTimeTo = dateTimePicker2.Value;
            }
            else
            {
                AlertReceivingTimeFrom = dateTimePicker1.MinDate;
                AlertReceivingTimeTo = dateTimePicker2.MaxDate;
            }
            FiltersChanged(this, EventArgs.Empty);
        }

        #region Messages filters
        private void NonCloseAlertsChBox_CheckedChanged(object sender, EventArgs e)
        {
            if (NonCloseAlertsChBox.Checked) NonClosedAlerts = true;
            else NonClosedAlerts = false;
            FiltersChanged(this, EventArgs.Empty);
        }

        private void ClosedAlertsChBox_CheckedChanged(object sender, EventArgs e)
        {
            if (ClosedAlertsChBox.Checked) ClosedAlerts = true;
            else ClosedAlerts = false;
            FiltersChanged(this, EventArgs.Empty);
        } 
        #endregion
    }
}
