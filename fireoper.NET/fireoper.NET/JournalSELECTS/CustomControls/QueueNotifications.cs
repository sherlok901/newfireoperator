﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using fireoper.NET.SharedClasses;
using System.Data.SqlClient;
using System.Security.Permissions;

namespace fireoper.NET.JournalSELECTS.CustomControls
{
    public partial class QueueNotifications : UserControl
    {
        FireLinqDataContext fl;
        private Filters filters1;

        private int currentPage = 1;
        private int pageNum;
        SqlConnection _connect;
        SqlCommand _command;
        SqlDependency _dependency;

        public QueueNotifications()
        {
            Dock = DockStyle.Fill;
            //this.VerticalScroll=new VScrollProperties(
            InitializeComponent();

            fl = new FireLinqDataContext(DataAccess.ConnectString);
            //this.SuspendLayout();

            filters1 = new Filters();
            filters1.Size = new Size(649, 200);
            filters1.FiltersChanged += Filters__FiltersChanged;
            filters1.Dock = DockStyle.Top;

            AlertsLabel1.Font = new Font(new FontFamily("Arial"), 12, FontStyle.Bold);

            CloseAlertsBtn1.Text = "Закрити тривогу";
            CloseAlertsBtn1.Height = 40;
            CloseAlertsBtn1.Width = 180;
            CloseAlertsBtn1.Font = new Font(new FontFamily("Arial"), 8, FontStyle.Bold);
            CloseAlertsBtn1.Click += CloseAlertsBtn_Click;
            //CloseAlertsBtn.Margin = new System.Windows.Forms.Padding(20, 0, 0, 0);


            dataGridViewAlerts1.CellClick += dg1Alerts_CellClick;
            dataGridViewAlerts1.Height = 400;
            //dataGridViewAlerts1.Anchor = AnchorStyles.Left | AnchorStyles.Right;

            dataGridViewActions1.Height = 250;

            DoneActionsLabl.Text = "Виконані дії";
            DoneActionsLabl.Font = new Font(new FontFamily("Arial"), 8, FontStyle.Regular);
            DoneActionsLabl.Width = 150;

            tableLayoutPanel1.Controls.Add(filters1, 0, 0);

            //Fill DataGridView with fresh info!
            RefreshQuery();

            comboBox1.SelectedIndex = 0;

            //viewedRowslabel1
            //
            //this.ResumeLayout(false);

            // Слідкуєм за залоченням тривог
            SenderToLockedAlert();
        }
        // Клік по datagridview
        void dg1Alerts_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dataGridViewAlerts1.RowCount == 0) return;

            AlertsJournalBean alarm_job_journal_ = (dataGridViewAlerts1.Rows[dataGridViewAlerts1.CurrentCellAddress.Y].DataBoundItem as AlertsJournalBean);
            if (alarm_job_journal_ == null) return;

            if (!alarm_job_journal_.ReceivedTime.Equals(null))
            {
                dataGridViewActions1.DataSource =
                    fl.alarm_action_journal_view.Where(c => c.AlarmJobJournalGuid.Equals(alarm_job_journal_.RowGuid))
                        .Select(c => new AlarmActionJournalBean
                        {
                            
                            Id = c.Id,
                           
                            DTStamp = c.DTStamp,
                            ActionName = c.ActionName,
                            AdresseeName = c.AddresseeName,
                            AlarmActionResultName = c.AlarmActionResultName,
                            PersonnelName = c.PersonnelName
                        });
            }
        }

        /// <summary>
        /// Кнопка закрити тривоги
        /// </summary>
        void CloseAlertsBtn_Click(object sender, EventArgs e)
        {
            //список guid-ов выделеных строк
            List<Guid> currentJournal = (dataGridViewAlerts1.SelectedRows.Cast<DataGridViewRow>()
                .Select(r => r.DataBoundItem)).OfType<AlertsJournalBean>()
                .Select(alertsJournalBean => alertsJournalBean.RowGuid).ToList();

            //get guid of current alarm_job_journal
            DialogResult dialogResult = new ResultForm(currentJournal).ShowDialog();

            //розлочити тривоги даним користувачем
            //id залочених тривог
            List<int> listId =
                fl.alarm_job_journal.Where(n => currentJournal.Contains(n.rowguid)).Select(n => n.Id).ToList();
            // Залочені тривоги в табл. EditedAlertsByUser
            List<EditedAlertsByUser> editedAlertsByUser =
                fl.EditedAlertsByUser.Where(n => n.SystemUser == DataAccess.UserLogin && listId.Contains(n.AlarmJobJournalID)).ToList();
            fl.EditedAlertsByUser.DeleteAllOnSubmit(editedAlertsByUser);
            fl.SubmitChanges();

            Thread.Sleep(100);
            //refresh datagridview
            //Filters__FiltersChanged(null, EventArgs.Empty);                  
        }

        //query for filtering alerts
        void Filters__FiltersChanged(object sender, EventArgs e)
        {
            RefreshQuery(true);
            label1.Text = currentPage + "/" + pageNum;
        }

        void RefreshQuery(bool resetCurrPage = false)
        {
            if (resetCurrPage) currentPage = 1;

            int rowCount = comboBox1.SelectedIndex != -1 ? Convert.ToInt32(comboBox1.Items[comboBox1.SelectedIndex]) : 0;

            double allRowsCount = (from c in fl.alarm_job_journal_view
                                   where
                                       //alerts aren't closed
                                   (
                                   (filters1.ClosedAlerts && !filters1.NonClosedAlerts) ?
                                       //only non closed alerts
                                   c.ClosedTime != null
                                   :
                                   (
                                    (!filters1.ClosedAlerts && filters1.NonClosedAlerts) ?
                                       //only closed alerts
                                    c.ClosedTime.Equals(null)
                                    : c.ClosedTime != null || c.ClosedTime.Equals(null)
                                   )
                                   )
                                       //filtering by received time
                                   && (c.ReceivedTime >= filters1.AlertReceivingTimeFrom)
                                   && (c.ReceivedTime <= filters1.AlertReceivingTimeTo)
                                       //guard objects
                                   && (
                                   filters1.SelectedGuardObjects.Count > 0 ?
                                    filters1.SelectedGuardObjects.Contains(c.GuardObjectId.Value)
                                    : c.GuardObjectId.Value.Equals(null) || c.GuardObjectId.Value >= 0
                                   )
                                   && (
                                   filters1.SelectedAlarmCategoryId.Count > 0 ?
                                    filters1.SelectedAlarmCategoryId.Contains((int)c.AlarmCategoryId)
                                    : c.AlarmCategoryId >= 0
                                   )


                                   orderby c.ReceivedTime descending
                                   select new AlertsJournalBean
                                   {
                                       Id = c.Id,
                                       ReceivedTime = String.Format("{0:d/M/yyyy HH:mm:ss}", c.ReceivedTime),
                                       ClosedTime = (c.ClosedTime == null) ? null : String.Format("{0:d/M/yyyy HH:mm:ss}", c.ClosedTime.Value),
                                       ObjectNumber = c.ObjectNumber,
                                       ObjectName = c.ObjectName,
                                       PremisesName = c.PremisesName,
                                       Address = c.Address,
                                       ReasonName = c.ReasonName,
                                       AlarmCategoryName = c.AlarmCategoryName,
                                       RowGuid = c.rowguid
                                   })
                                   .Count()
                                   ;
            pageNum = (int)Math.Floor(allRowsCount / rowCount);

            dataGridViewAlerts1.DataSource = (from c in fl.alarm_job_journal_view
                                              where
                                                  //alerts aren't closed
                                              (
                                              (filters1.ClosedAlerts && !filters1.NonClosedAlerts) ?
                                                  //only non closed alerts
                                              c.ClosedTime != null
                                              :
                                              (
                                               (!filters1.ClosedAlerts && filters1.NonClosedAlerts) ?
                                                  //only closed alerts
                                               c.ClosedTime.Equals(null)
                                               : c.ClosedTime != null || c.ClosedTime.Equals(null)
                                              )
                                              )
                                                  //filtering by received time
                                              && (c.ReceivedTime >= filters1.AlertReceivingTimeFrom)
                                              && (c.ReceivedTime <= filters1.AlertReceivingTimeTo)
                                                  //guard objects
                                              && (
                                              filters1.SelectedGuardObjects.Count > 0 ?
                                               filters1.SelectedGuardObjects.Contains(c.GuardObjectId.Value)
                                               : c.GuardObjectId.Value.Equals(null) || c.GuardObjectId.Value >= 0
                                              )
                                              && (
                                              filters1.SelectedAlarmCategoryId.Count > 0 ?
                                               filters1.SelectedAlarmCategoryId.Contains((int)c.AlarmCategoryId)
                                               : c.AlarmCategoryId >= 0
                                              )


                                              orderby c.ReceivedTime descending
                                              select new AlertsJournalBean
                                              {
                                                  Id = c.Id,
                                                  ReceivedTime = String.Format("{0:d/M/yyyy HH:mm:ss}", c.ReceivedTime),
                                                  ClosedTime = (c.ClosedTime == null) ? null : String.Format("{0:d/M/yyyy HH:mm:ss}", c.ClosedTime.Value),
                                                  ObjectNumber = c.ObjectNumber,
                                                  ObjectName = c.ObjectName,
                                                  PremisesName = c.PremisesName,
                                                  Address = c.Address,
                                                  ReasonName = c.ReasonName,
                                                  AlarmCategoryName = c.AlarmCategoryName,
                                                  RowGuid = c.rowguid
                                              })
                                   .Skip(currentPage * rowCount).Take(rowCount)
                                   ;

            //Изменяем высоту
            if (dataGridViewAlerts1.Rows.Count > 0)
            {
                int rowHeight = dataGridViewAlerts1.Rows[0].Height;
                dataGridViewAlerts1.Height = rowCount * rowHeight + dataGridViewAlerts1.ColumnHeadersHeight + 20;
            }

            foreach (DataGridViewRow row in dataGridViewAlerts1.Rows)
            {
                AlertsJournalBean alertsJournalBean = (row.DataBoundItem as AlertsJournalBean);
                if (alertsJournalBean.AlarmCategoryName.ToLower().Contains("пожеж"))
                    row.DefaultCellStyle.BackColor = Color.Crimson;
                else if (alertsJournalBean.AlarmCategoryName.ToLower().Contains("тривог"))
                    row.DefaultCellStyle.BackColor = Color.Coral;
                else if (alertsJournalBean.AlarmCategoryName.ToLower().Contains("несправ"))
                    row.DefaultCellStyle.BackColor = Color.Yellow;
                else row.DefaultCellStyle.BackColor = Color.White;
            }
        }

        // Попередня сторінка
        private void button1_Click(object sender, EventArgs e)
        {
            if (currentPage > 1)
            {
                --currentPage;
                label1.Text = currentPage + "/" + pageNum;
                RefreshQuery();
            }
        }

        //Наступна сторінка
        private void button2_Click(object sender, EventArgs e)
        {
            if (currentPage < pageNum)
            {
                currentPage++;
                label1.Text = currentPage + "/" + pageNum;
                RefreshQuery();
            }
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            RefreshQuery();
            label1.Text = currentPage + "/" + pageNum;

        }

        private void SenderToLockedAlert()
        {
            //check permission
            SqlClientPermission permission = new SqlClientPermission(PermissionState.Unrestricted);
            try { permission.Demand(); }
            catch (Exception) { }

            _connect = new SqlConnection(DataAccess.ConnectString);
            _connect.Open();
            _command = new SqlCommand(@"SELECT [id]
      ,[AlarmJobJournalID]
      ,[SystemUser]
        FROM [dbo].[EditedAlertsByUser]", _connect);
            _dependency = new SqlDependency(_command);

            SqlDependency.Start(DataAccess.ConnectString);
            _dependency.OnChange += _dependency_OnChange;
            SqlDataReader reader = _command.ExecuteReader();
        }

        void _dependency_OnChange(object sender, SqlNotificationEventArgs e)
        {
            //коли в таблицю доданий запис(залочена тривога користувачем)
            if ((e.Info.Equals(SqlNotificationInfo.Insert) ||
                 e.Info.Equals(SqlNotificationInfo.Delete)
                )
                && e.Source.Equals(SqlNotificationSource.Data)
                && e.Type.Equals(SqlNotificationType.Change))
            {
                Filters__FiltersChanged(null, EventArgs.Empty);
                Thread.Sleep(100);
                // id всіх залочених тривог
                List<int> lockedAlarmJobJournals = fl.EditedAlertsByUser.Select(n => n.alarm_job_journal.Id).ToList();

                //були залочені тривоги; якщо вони є в нашій таблиці, показати їх
                //якщо залочених тривог немає в нашій таблиці - фарбуєм тривоги в нашій таблиці білим кольором
                ChangeRowColor(lockedAlarmJobJournals);
            }

            //знову підписуємось на нові тривоги
            if (this.InvokeRequired)
                this.BeginInvoke(new MethodInvoker(SenderToLockedAlert));
            else
                SenderToLockedAlert();
            SqlDependency dep = sender as SqlDependency;
            dep.OnChange -= _dependency_OnChange;
        }

        /// <summary>
        /// Показати залочені тривоги користувачами
        /// </summary>
        /// <param name="alarmJobJournals"></param>
        void ChangeRowColor(List<int> lockedAlarmJobJournals)
        {
            foreach (DataGridViewRow row in dataGridViewAlerts1.Rows)
            {
                int id = (row.DataBoundItem as AlertsJournalBean).Id;
                if (lockedAlarmJobJournals.Contains(id))
                    row.DefaultCellStyle.BackColor = Color.DarkGray;
                else row.DefaultCellStyle.BackColor = Color.White;
            }
        }

        private void DoneActionsLabl_Click(object sender, EventArgs e)
        {

        }

        private void tableLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
