﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace fireoper.NET.JournalSELECTS.CustomControls
{
    public partial class AlertsFaults : UserControl
    {
        FireLinqDataContext fl;

        public AlertsFaults(string ConnStr)
        {
            InitializeComponent();
            this.Dock = DockStyle.Fill;

            fl = new FireLinqDataContext(ConnStr);
            dataGridView1.DataSource = from c in fl.alarm_job_journal
                                       where c.ClosedTime.Equals(null)
                                       orderby c.ReceivedTime
                                       select c;
            
            if (dataGridView1.Rows.Count > 0)
            {
                string guid = getRowGUID(0);
                FillAlertsFaultsDetailGrid(guid);
                FillDiiDataGridView(guid);
            }
        }

        private void FillAlertsFaultsDetailGrid(string guid)
        {
            dataGridView2.DataSource = from c in fl.alarm_request_for_load_view
                                       where c.AlarmJobJournalGuid.Value.ToString().Equals(guid)
                                       select c;
        }

        private void FillDiiDataGridView(string guid)
        {
            dataGridView3.DataSource = from c in fl.alarm_action_journal_view
                                          where c.AlarmJobJournalGuid.ToString().Equals(guid)
                                          select c;
        }

        void dataGridView1_CellClick(object sender, System.Windows.Forms.DataGridViewCellEventArgs e)
        {
            if (e.RowIndex == -1) return;
            string guid = getRowGUID(e.RowIndex);
            FillAlertsFaultsDetailGrid(guid);
            FillDiiDataGridView(guid);
        }

        string getRowGUID(int RowIndex)
        {
            return (dataGridView1.Rows[RowIndex].DataBoundItem as alarm_job_journal).rowguid.ToString();
        }
    }
}
