﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace fireoper.NET.JournalSELECTS.CustomControls
{
    class DutyEmployersJournal : UnivarsalJournalControl
    {
        FireLinqDataContext fl = new FireLinqDataContext(DataAccess.ConnectString);
        public DutyEmployersJournal()
        {
            dataGridView1.DataSource = from c in fl.shift_view orderby c.OnDate descending, c.IsOnline.Value select c;
            dataGridView1.CellClick += new DataGridViewCellEventHandler(dataGridView1_CellClick);
            //заповнення datagridview2 даними з [shift_time_detail] взяті по id з shift_view
            if (dataGridView1.Rows.Count < 1) return;
            int id = (dataGridView1.Rows[0].DataBoundItem as shift_view).Id;
            getTimeDetailByShift_viewID(id);
            tabPage1.Text = "Деталізація";
        }

        void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex <= -1) return;
            int id = (dataGridView1.Rows[e.RowIndex].DataBoundItem as shift_view).Id;
            getTimeDetailByShift_viewID(id);
        }

        //заповнення datagridview2 даними з [shift_time_detail] взяті по id з shift_view
        private void getTimeDetailByShift_viewID(int id)
        {
            dataGridView2.DataSource = from c in fl.shift_time_detail where c.ShiftId == id orderby c.Id select c;
        }
    }
}
