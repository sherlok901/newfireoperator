﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using fireoper.NET;

namespace fireoper.NET.JournalSELECTS.CustomControls
{
    class DutyJournalTechGroup:UnivarsalJournalControl
    {
        FireLinqDataContext fl;

        internal DutyJournalTechGroup(string ConnStr)
        {
            fl = new FireLinqDataContext(ConnStr);
            dataGridView1.DataSource = from c in fl.tech_group_journal_view orderby c.OnDate descending select c;
            dataGridView1.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(dataGridView1_CellClick);
            if (dataGridView1.Rows.Count > 0)
            {
                //Вкладка Склад групи
                int id = getId(0);
                fillSkladGroup(id);
            }
        }

        private void fillSkladGroup(int id)
        {
            dataGridView2.DataSource = from c in fl.tech_group_personnel_journal_view
                                       where c.TechGroupJournalId == id
                                       orderby c.Id
                                       select c;
        }

        void dataGridView1_CellClick(object sender, System.Windows.Forms.DataGridViewCellEventArgs e)
        {
            if (dataGridView1.Rows.Count > 0)
            {
                //Вкладка Склад групи
                int id = getId(e.RowIndex);
                fillSkladGroup(id);
            }
        }
        int getId(int rowIndex)
        {
            return (dataGridView1.Rows[rowIndex].DataBoundItem as tech_group_journal_view).TechGroupId;
        }
    }
}
