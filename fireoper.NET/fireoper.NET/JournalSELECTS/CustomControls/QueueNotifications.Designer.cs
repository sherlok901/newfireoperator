﻿namespace fireoper.NET.JournalSELECTS.CustomControls
{
    partial class QueueNotifications
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.AlertsLabel1 = new System.Windows.Forms.Label();
            this.CloseAlertsBtn1 = new System.Windows.Forms.Button();
            this.dataGridViewActions1 = new System.Windows.Forms.DataGridView();
            this.dataGridViewAlerts1 = new System.Windows.Forms.DataGridView();
            this.DoneActionsLabl = new System.Windows.Forms.Label();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.viewedRowslabel1 = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.button1 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewActions1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewAlerts1)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // AlertsLabel1
            // 
            this.AlertsLabel1.AutoSize = true;
            this.AlertsLabel1.Location = new System.Drawing.Point(3, 0);
            this.AlertsLabel1.Name = "AlertsLabel1";
            this.AlertsLabel1.Size = new System.Drawing.Size(49, 13);
            this.AlertsLabel1.TabIndex = 0;
            this.AlertsLabel1.Text = "Тривоги";
            // 
            // CloseAlertsBtn1
            // 
            this.CloseAlertsBtn1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.CloseAlertsBtn1.Location = new System.Drawing.Point(212, 134);
            this.CloseAlertsBtn1.Name = "CloseAlertsBtn1";
            this.CloseAlertsBtn1.Size = new System.Drawing.Size(75, 23);
            this.CloseAlertsBtn1.TabIndex = 2;
            this.CloseAlertsBtn1.Text = "button1";
            this.CloseAlertsBtn1.UseVisualStyleBackColor = true;
            // 
            // dataGridViewActions1
            // 
            this.dataGridViewActions1.AllowUserToAddRows = false;
            this.dataGridViewActions1.AllowUserToDeleteRows = false;
            this.dataGridViewActions1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridViewActions1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewActions1.Location = new System.Drawing.Point(3, 176);
            this.dataGridViewActions1.Name = "dataGridViewActions1";
            this.dataGridViewActions1.ReadOnly = true;
            this.dataGridViewActions1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewActions1.Size = new System.Drawing.Size(494, 15);
            this.dataGridViewActions1.TabIndex = 3;
            // 
            // dataGridViewAlerts1
            // 
            this.dataGridViewAlerts1.AllowUserToAddRows = false;
            this.dataGridViewAlerts1.AllowUserToDeleteRows = false;
            this.dataGridViewAlerts1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridViewAlerts1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewAlerts1.Location = new System.Drawing.Point(3, 16);
            this.dataGridViewAlerts1.Name = "dataGridViewAlerts1";
            this.dataGridViewAlerts1.ReadOnly = true;
            this.dataGridViewAlerts1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewAlerts1.Size = new System.Drawing.Size(494, 64);
            this.dataGridViewAlerts1.TabIndex = 4;
            // 
            // DoneActionsLabl
            // 
            this.DoneActionsLabl.AutoSize = true;
            this.DoneActionsLabl.Location = new System.Drawing.Point(3, 160);
            this.DoneActionsLabl.Name = "DoneActionsLabl";
            this.DoneActionsLabl.Size = new System.Drawing.Size(35, 13);
            this.DoneActionsLabl.TabIndex = 5;
            this.DoneActionsLabl.Text = "label2";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.AutoScroll = true;
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.dataGridViewAlerts1, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.AlertsLabel1, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.dataGridViewActions1, 0, 6);
            this.tableLayoutPanel1.Controls.Add(this.DoneActionsLabl, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.CloseAlertsBtn1, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.flowLayoutPanel1, 0, 3);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 7;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.Size = new System.Drawing.Size(500, 400);
            this.tableLayoutPanel1.TabIndex = 7;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.viewedRowslabel1);
            this.flowLayoutPanel1.Controls.Add(this.comboBox1);
            this.flowLayoutPanel1.Controls.Add(this.button1);
            this.flowLayoutPanel1.Controls.Add(this.label1);
            this.flowLayoutPanel1.Controls.Add(this.button2);
            this.flowLayoutPanel1.Location = new System.Drawing.Point(3, 86);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(494, 42);
            this.flowLayoutPanel1.TabIndex = 6;
            // 
            // viewedRowslabel1
            // 
            this.viewedRowslabel1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.viewedRowslabel1.AutoSize = true;
            this.viewedRowslabel1.Location = new System.Drawing.Point(3, 7);
            this.viewedRowslabel1.Name = "viewedRowslabel1";
            this.viewedRowslabel1.Padding = new System.Windows.Forms.Padding(0, 8, 0, 0);
            this.viewedRowslabel1.Size = new System.Drawing.Size(59, 21);
            this.viewedRowslabel1.TabIndex = 1;
            this.viewedRowslabel1.Text = "Показати:";
            // 
            // comboBox1
            // 
            this.comboBox1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.comboBox1.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "10",
            "15",
            "20",
            "40"});
            this.comboBox1.Location = new System.Drawing.Point(68, 7);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(121, 21);
            this.comboBox1.TabIndex = 2;
            this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // button1
            // 
            this.button1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.button1.AutoSize = true;
            this.button1.Location = new System.Drawing.Point(195, 3);
            this.button1.MaximumSize = new System.Drawing.Size(100, 40);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(30, 30);
            this.button1.TabIndex = 0;
            this.button1.Text = "<";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(231, 0);
            this.label1.Name = "label1";
            this.label1.Padding = new System.Windows.Forms.Padding(0, 13, 0, 0);
            this.label1.Size = new System.Drawing.Size(45, 29);
            this.label1.TabIndex = 3;
            this.label1.Text = "label1";
            // 
            // button2
            // 
            this.button2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.button2.Location = new System.Drawing.Point(282, 3);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(30, 30);
            this.button2.TabIndex = 4;
            this.button2.Text = ">";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // QueueNotifications
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.Controls.Add(this.tableLayoutPanel1);
            this.MinimumSize = new System.Drawing.Size(500, 400);
            this.Name = "QueueNotifications";
            this.Size = new System.Drawing.Size(500, 400);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewActions1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewAlerts1)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.flowLayoutPanel1.ResumeLayout(false);
            this.flowLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label AlertsLabel1;
        
        private System.Windows.Forms.Button CloseAlertsBtn1;
        private System.Windows.Forms.DataGridView dataGridViewActions1;
        private System.Windows.Forms.DataGridView dataGridViewAlerts1;
        private System.Windows.Forms.Label DoneActionsLabl;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label viewedRowslabel1;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button2;




    }
}
