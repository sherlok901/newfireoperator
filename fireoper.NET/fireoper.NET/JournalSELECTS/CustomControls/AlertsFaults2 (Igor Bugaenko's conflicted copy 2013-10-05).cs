﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace fireoper.NET.JournalSELECTS.CustomControls
{
    public partial class AlertsFaults2 : UserControl
    {
        FireLinqDataContext fl;

        internal AlertsFaults2(string ConnStr)
        {            
            InitializeComponent();
            dataGridView1.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dataGridView1.AllowUserToAddRows = false;
            this.Dock = DockStyle.Fill;
            fl = new FireLinqDataContext(ConnStr);
          
            var query2 =
                from alarm_job_journal in fl.alarm_job_journal
                group alarm_job_journal by new
                {
                    alarm_job_journal.AlarmCategoryId
                } into g
                select new AlarnCategoryBean
                {
                    id = g.Key.AlarmCategoryId,                   
                    AlarmCategorName=fl.s_alarm_category.Where(s=>s.Id==g.Key.AlarmCategoryId).Single().Name,
                    NonClosedAlarnCount = g.Count(p => p.AlarmCategoryId != null)
                };
            //========================
            dataGridView1.DataSource = query2;
            dataGridView2.DataSource = from c in fl.alarm_job_journal
                                       where c.ClosedTime.Equals(null)
                                       orderby c.ReceivedTime 
                                       select c;
        }

        private void tabControl1_MouseHover(object sender, EventArgs e)
        {
           // tabControl1.Height = this.Height - 20;
        }

        private void tabControl1_MouseLeave(object sender, EventArgs e)
        {
            //tabControl1.Height = 21;
        }        
    }
    internal class AlarnCategoryBean
    {
        [System.ComponentModel.Browsable(false)]
        public int id { get; set; }
        [System.ComponentModel.ReadOnly(true)]
        [System.ComponentModel.DisplayName("Категорія")]
        public string AlarmCategorName { get; set; }
        [System.ComponentModel.ReadOnly(true)]
        [System.ComponentModel.DisplayName("Кількість не виконаних")]
        public int NonClosedAlarnCount { get; set; }
    }
}
