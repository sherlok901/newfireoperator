﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using fireoper.NET.SharedClasses;

namespace fireoper.NET.JournalSELECTS.CustomControls
{
    class FiresJournal:UnivarsalJournalControl
    {
        DataGridView dg3;
        FireLinqDataContext fl;

        internal FiresJournal(string ConnectStr)
        {
            fl = new FireLinqDataContext(ConnectStr);
            dataGridView1.DataSource = from c in fl.alarm_job_journal_view
                                       where c.AlarmCategoryId == 4
                                       orderby c.ReceivedTime descending
                                       select c;
            dataGridView1.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(dataGridView1_CellClick);
            //Сповіщення
            tabPage1.Text = "Сповіщення";
            if (dataGridView1.Rows.Count < 1) return;
            string guid = getIDRowValue(0);
            fillAlertsTabPage(guid);
            //Дії
            TabPage tp2 = new TabPage("Дії");
            dg3 = ControlsFabric.getNewDataGridView();
            fillDiiTabPage(guid);
            tp2.Controls.Add(dg3);
            tabControl1.Controls.Add(tp2);
        }

        void dataGridView1_CellClick(object sender, System.Windows.Forms.DataGridViewCellEventArgs e)
        {
            if (e.RowIndex <0) return;
            string guid = getIDRowValue(e.RowIndex);
            if (tabControl1.SelectedIndex == 0) fillAlertsTabPage(guid);
            else if (tabControl1.SelectedIndex == 1) fillDiiTabPage(guid);
        }
        //Сповіщення
        void fillAlertsTabPage(string guid)
        {
            dataGridView2.DataSource = from c in fl.alarm_request_journal_view
                             where c.AlarmJobJournalGuid.Value.ToString().Equals(guid)
                             orderby c.ReceiveDTStamp
                             select c;
        }
        //Дії
        void fillDiiTabPage(string guid)
        {
            dg3.DataSource = from c in fl.alarm_action_journal_view
                                       where c.AlarmJobJournalGuid.ToString().Equals(guid)
                                       orderby c.DTStamp
                                       select c;
        }
        string getIDRowValue(int rowIndex)
        {
            string result = null;
            if (dataGridView1.Rows.Count > 0)
                result = (dataGridView1.Rows[rowIndex].DataBoundItem as alarm_job_journal_view).rowguid.ToString();
            return result;
        }
    }
}
