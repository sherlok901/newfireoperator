﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace fireoper.NET.JournalSELECTS.CustomControls
{
    public partial class UnivarsalJournalControl : UserControl
    {
        //linq datacontext
        protected internal FireLinqDataContext fl;

        public UnivarsalJournalControl()
        {
            InitializeComponent();
            //настройки datagridview
            dataGridView1.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            
        }

        private void UnivarsalJournalControl_Load(object sender, EventArgs e)
        {
            this.Dock = DockStyle.Fill;
        }
    }
}
