﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Windows.Controls;

namespace fireoper.NET.JournalSELECTS.CustomControls
{
    public partial class AlertJournalControl : System.Windows.Forms.UserControl
    {
        FireLinqDataContext fl;
        //BindingSource bs = new BindingSource();
        IQueryable<alarm_request_journal_view> alarmsList;
        IQueryable<alarm_action_journal_view> actionsList;
        //System.Windows.Forms.DataGrid dataG = new System.Windows.Forms.DataGrid();

        public AlertJournalControl(string ConnString)
        {
            InitializeComponent();
            fl = new FireLinqDataContext(ConnString);
            //настройки datagridview
            dataGridView1.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dataGridView1.AllowUserToAddRows =
                 dataGridView2.AllowUserToAddRows =
                 dataGridView3.AllowUserToAddRows =
                 false;       
            dataGridView1.Dock =
                dataGridView2.Dock =
                dataGridView3.Dock =
                System.Windows.Forms.DockStyle.Fill;
            this.Dock = System.Windows.Forms.DockStyle.Fill;

            tabControl1.SelectedIndexChanged += new EventHandler(tabControl1_SelectedIndexChanged);

            //заповнення datagridview Журнал тривог
             var AlarmJourVar = from c in fl.alarm_job_journal_view
                                       where c.AlarmCategoryId != 2 && c.AlarmCategoryId != 4
                                       orderby c.ReceivedTime descending
                                       select c;
             //dataG.DataSource = AlarmJourVar;
             //dataG.Dock = DockStyle.Fill;
             //splitContainer1.Panel1.Controls.Add(dataG);
             dataGridView1.DataSource = AlarmJourVar;
            //this.alarm_job_journal_viewTableAdapter.FillBy(this.fireDataSet1.alarm_job_journal_view);


             dataGridView1.CellClick += new DataGridViewCellEventHandler(dataGridView1_CellClick);           
            //загрузка даних для Сповіщення(datagridview2), Дії(datagridview3)
             if (dataGridView1.Rows.Count <= 0) return;
             string guid = (dataGridView1.Rows[0].DataBoundItem as alarm_job_journal_view).rowguid.ToString();
             FillSpovischennyaDataGridView(guid);
             FillDiiDataGridView(guid);
        }
        //при виборі іншої Таб підгружати для неї дані в залежності від обраного рядка у верній datagridview
        void tabControl1_SelectedIndexChanged(object sender, EventArgs e)
        {
            dataGridView1_CellClick(dataGridView1,new DataGridViewCellEventArgs(dataGridView1.CurrentCellAddress.X,dataGridView1.CurrentCellAddress.Y));
        }      

        void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex == -1) return;
            string guid = (dataGridView1.Rows[e.RowIndex].DataBoundItem as alarm_job_journal_view).rowguid.ToString();
            //string guid= dataGridView1.Rows[e.RowIndex].Cells[16].Value.ToString();
            if (tabControl1.SelectedIndex == 0)
            {
                FillSpovischennyaDataGridView(guid);
            }
            else if (tabControl1.SelectedIndex == 1)
            {
                FillDiiDataGridView(guid);
            }            
        }

        private void FillDiiDataGridView(string guid)
        {
            actionsList = from c in fl.alarm_action_journal_view
                          where c.AlarmJobJournalGuid.ToString() == guid
                          orderby c.DTStamp
                          select c;
            dataGridView3.DataSource = actionsList;
        }

        private void FillSpovischennyaDataGridView(string guid)
        {
            alarmsList = from c in fl.alarm_request_journal_view
                         where c.AlarmJobJournalGuid.Value.ToString() == guid
                         orderby c.ReceiveDTStamp.Value
                         select c;
            dataGridView2.DataSource = alarmsList;
        }
        
    }
}
