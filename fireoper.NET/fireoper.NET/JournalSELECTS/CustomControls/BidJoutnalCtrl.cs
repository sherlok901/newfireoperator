﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace fireoper.NET.JournalSELECTS.CustomControls
{
    class BidJoutnalCtrl : UnivarsalJournalControl
    {
        FireLinqDataContext fl;
        DataGridView dg2;
        DataGridView dg3;
        DataGridView dg4;
        DataGridView dg5;
        TabPage tp2;
        TabPage tp3;
        TabPage tp4;
        TabPage tp5;

        public BidJoutnalCtrl()
        {
            this.Dock = DockStyle.Fill;
            fl = new FireLinqDataContext(DataAccess.ConnectString);
            tp2 = new TabPage("Матриця сповіщень зі сповіщувачів");
            tp3 = new TabPage("Матриця сповіщень зі шлейфів");
            tp4 = new TabPage("Матриця сповіщень з ППК");
            tp5 = new TabPage("Сповіщення");
            tabControl1.TabPages.Add(tp2);
            tabControl1.TabPages.Add(tp3);
            tabControl1.TabPages.Add(tp4);
            tabControl1.TabPages.Add(tp5);
            dataGridView1.CellClick += new DataGridViewCellEventHandler(dataGridView1_CellClick);
            //Журнал Заявки
            dataGridView1.DataSource = from c in fl.demand_view orderby c.CreateDate descending select c;
            if (dataGridView1.Rows.Count <= 0) return;
            int demand_view_id = getIDRowValue(0);
            //вкладка Дії
            tabPage1.Text = "Дії";
            fillDiiTabPage(demand_view_id);
            //вкладка Матриця сповищень зи сповищувачив
            dg2 = getNewDataGridView();
            fillMatrixAlertsFromAlertsTabPage(demand_view_id);
            tp2.Controls.Add(dg2);
            //вкладка Матриця сповіщень зі шлейфів
            dg3 = getNewDataGridView();
            fillMatrixAlertsFromShleifTabPage(demand_view_id);
            tp3.Controls.Add(dg3);
            //вкладка Матриця сповіщень з ППК
            dg4 = getNewDataGridView();
            fillMatrixAlertsFromPPKTabPage(demand_view_id);
            tp4.Controls.Add(dg4);
            //Сповіщення
            dg5 = getNewDataGridView();
            fillAlertsTabPage(demand_view_id);
            tp5.Controls.Add(dg5);
        }

        void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if ((sender is DataGridView)&& e.RowIndex>-1)
            {
                int id = getIDRowValue(e.RowIndex);
                if (tabControl1.SelectedIndex == 0)
                {
                    fillDiiTabPage(id);
                }
                else if (tabControl1.SelectedIndex == 1)
                {
                    fillMatrixAlertsFromAlertsTabPage(id);
                }
                else if (tabControl1.SelectedIndex == 2) fillMatrixAlertsFromShleifTabPage(id);
                else if (tabControl1.SelectedIndex == 3) fillMatrixAlertsFromPPKTabPage(id);
                else if (tabControl1.SelectedIndex == 4) fillAlertsTabPage(id);

            }
        }

        //приватні методи заповнення datagridview по TAb в зележності від id табл. demand_view
        void fillDiiTabPage(int id)
        {
            dataGridView2.DataSource = from c in fl.demand_action_journal_view
                                       where c.DemandId == id
                                       orderby c.CompleteDate, c.Priority
                                       select c;
        }
        void fillMatrixAlertsFromAlertsTabPage(int id)
        {
            dg2.DataSource = from c in fl.to_matrix_detector_view
                             where c.DemandId == id
                             orderby c.Id
                             select c;
        }
        void fillMatrixAlertsFromShleifTabPage(int id)
        {
            dg3.DataSource = from c in fl.to_matrix_view
                             where c.DemandId == id
                             orderby c.Id
                             select c;
        }
        void fillMatrixAlertsFromPPKTabPage(int id)
        {
            dg4.DataSource = from c in fl.to_matrix_ppk_view
                             where c.DemandId == id
                             orderby c.Id
                             select c;
        }
        void fillAlertsTabPage(int id)
        {
            dg5.DataSource = from c in fl.alarm_request_journal_view
                             where c.DemandId == id
                             orderby c.ReceiveDTStamp
                             select c;
        }
        int getIDRowValue(int rowIndex)
        {
            return (dataGridView1.Rows[rowIndex].DataBoundItem as demand_view).Id;
        }
        DataGridView getNewDataGridView()
        {
            DataGridView dg = new DataGridView();
            //настройки datagridview
            dg.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dg.AllowUserToAddRows = false;
            //dg.ColumnHeadersHeightSizeMode =
            //    System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            //dg.AutoSizeColumnsMode =
            //    DataGridViewAutoSizeColumnsMode.AllCells;
            //dg.RowHeadersWidthSizeMode =
            //    DataGridViewRowHeadersWidthSizeMode.AutoSizeToAllHeaders;
            dg.Dock = DockStyle.Fill;
            return dg;
        }
    }
}
