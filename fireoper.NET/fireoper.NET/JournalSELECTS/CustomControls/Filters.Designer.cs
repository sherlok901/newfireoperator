﻿namespace fireoper.NET.JournalSELECTS.CustomControls
{
    partial class Filters
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.FilterBtn = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.dataGridView1GuardObj = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.SelectAllcheckBox1 = new System.Windows.Forms.CheckBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.dateTimePicker2 = new System.Windows.Forms.DateTimePicker();
            this.TurnOnOffDataTimePicker = new System.Windows.Forms.CheckBox();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.dataGridView2AlarmCateg = new System.Windows.Forms.DataGridView();
            this.dataGridViewCheckBoxColumn1 = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.ClosedAlertsChBox = new System.Windows.Forms.CheckBox();
            this.NonCloseAlertsChBox = new System.Windows.Forms.CheckBox();
            this.button1 = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1GuardObj)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2AlarmCateg)).BeginInit();
            this.panel1.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.SuspendLayout();
            // 
            // FilterBtn
            // 
            this.FilterBtn.Image = global::fireoper.NET.Properties.Resources.filter_btn_open;
            this.FilterBtn.Location = new System.Drawing.Point(3, 3);
            this.FilterBtn.Name = "FilterBtn";
            this.FilterBtn.Size = new System.Drawing.Size(214, 37);
            this.FilterBtn.TabIndex = 0;
            this.FilterBtn.UseVisualStyleBackColor = true;
            this.FilterBtn.Click += new System.EventHandler(this.button1_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.dataGridView1GuardObj);
            this.groupBox1.Controls.Add(this.SelectAllcheckBox1);
            this.groupBox1.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBox1.Location = new System.Drawing.Point(3, 30);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(375, 273);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Обєкти:";
            // 
            // dataGridView1GuardObj
            // 
            this.dataGridView1GuardObj.AllowUserToAddRows = false;
            this.dataGridView1GuardObj.AllowUserToDeleteRows = false;
            this.dataGridView1GuardObj.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.ColumnHeader;
            this.dataGridView1GuardObj.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1GuardObj.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1});
            this.dataGridView1GuardObj.Location = new System.Drawing.Point(38, 51);
            this.dataGridView1GuardObj.Name = "dataGridView1GuardObj";
            this.dataGridView1GuardObj.RowHeadersVisible = false;
            this.dataGridView1GuardObj.RowHeadersWidth = 20;
            this.dataGridView1GuardObj.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1GuardObj.Size = new System.Drawing.Size(292, 214);
            this.dataGridView1GuardObj.TabIndex = 2;
            this.dataGridView1GuardObj.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            this.dataGridView1GuardObj.CurrentCellDirtyStateChanged += new System.EventHandler(this.dataGridView1_CurrentCellDirtyStateChanged);
            // 
            // Column1
            // 
            this.Column1.HeaderText = "Вибрати";
            this.Column1.Name = "Column1";
            this.Column1.Width = 75;
            // 
            // SelectAllcheckBox1
            // 
            this.SelectAllcheckBox1.AutoSize = true;
            this.SelectAllcheckBox1.Location = new System.Drawing.Point(38, 25);
            this.SelectAllcheckBox1.Name = "SelectAllcheckBox1";
            this.SelectAllcheckBox1.Size = new System.Drawing.Size(50, 22);
            this.SelectAllcheckBox1.TabIndex = 1;
            this.SelectAllcheckBox1.Text = "Всі";
            this.SelectAllcheckBox1.UseVisualStyleBackColor = true;
            this.SelectAllcheckBox1.CheckedChanged += new System.EventHandler(this.SelectAllcheckBox1_CheckedChanged);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.dateTimePicker2);
            this.groupBox2.Controls.Add(this.TurnOnOffDataTimePicker);
            this.groupBox2.Controls.Add(this.dateTimePicker1);
            this.groupBox2.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBox2.Location = new System.Drawing.Point(3, 309);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(375, 110);
            this.groupBox2.TabIndex = 3;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Дата прийому повідомлень:";
            // 
            // dateTimePicker2
            // 
            this.dateTimePicker2.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePicker2.Location = new System.Drawing.Point(59, 66);
            this.dateTimePicker2.Name = "dateTimePicker2";
            this.dateTimePicker2.Size = new System.Drawing.Size(217, 25);
            this.dateTimePicker2.TabIndex = 1;
            // 
            // TurnOnOffDataTimePicker
            // 
            this.TurnOnOffDataTimePicker.AutoSize = true;
            this.TurnOnOffDataTimePicker.Location = new System.Drawing.Point(38, 41);
            this.TurnOnOffDataTimePicker.Name = "TurnOnOffDataTimePicker";
            this.TurnOnOffDataTimePicker.Size = new System.Drawing.Size(15, 14);
            this.TurnOnOffDataTimePicker.TabIndex = 2;
            this.TurnOnOffDataTimePicker.UseVisualStyleBackColor = true;
            this.TurnOnOffDataTimePicker.CheckedChanged += new System.EventHandler(this.TurnOnOffDataTimePicker_CheckedChanged);
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePicker1.Location = new System.Drawing.Point(59, 33);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(217, 25);
            this.dateTimePicker1.TabIndex = 0;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.dataGridView2AlarmCateg);
            this.groupBox3.Controls.Add(this.checkBox1);
            this.groupBox3.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBox3.Location = new System.Drawing.Point(384, 30);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(384, 273);
            this.groupBox3.TabIndex = 4;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Категорії тривог:";
            // 
            // dataGridView2AlarmCateg
            // 
            this.dataGridView2AlarmCateg.AllowUserToAddRows = false;
            this.dataGridView2AlarmCateg.AllowUserToDeleteRows = false;
            this.dataGridView2AlarmCateg.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.ColumnHeader;
            this.dataGridView2AlarmCateg.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView2AlarmCateg.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewCheckBoxColumn1});
            this.dataGridView2AlarmCateg.Location = new System.Drawing.Point(47, 51);
            this.dataGridView2AlarmCateg.Name = "dataGridView2AlarmCateg";
            this.dataGridView2AlarmCateg.RowHeadersVisible = false;
            this.dataGridView2AlarmCateg.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView2AlarmCateg.Size = new System.Drawing.Size(292, 214);
            this.dataGridView2AlarmCateg.TabIndex = 3;
            this.dataGridView2AlarmCateg.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView2AlarmCateg_CellContentClick);
            this.dataGridView2AlarmCateg.CurrentCellDirtyStateChanged += new System.EventHandler(this.dataGridView2AlarmCateg_CurrentCellDirtyStateChanged);
            // 
            // dataGridViewCheckBoxColumn1
            // 
            this.dataGridViewCheckBoxColumn1.HeaderText = "Вибрати";
            this.dataGridViewCheckBoxColumn1.Name = "dataGridViewCheckBoxColumn1";
            this.dataGridViewCheckBoxColumn1.Width = 75;
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(47, 25);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(50, 22);
            this.checkBox1.TabIndex = 2;
            this.checkBox1.Text = "Всі";
            this.checkBox1.UseVisualStyleBackColor = true;
            this.checkBox1.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.AutoScroll = true;
            this.panel1.AutoSize = true;
            this.panel1.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.panel1.Controls.Add(this.groupBox4);
            this.panel1.Controls.Add(this.button1);
            this.panel1.Controls.Add(this.groupBox1);
            this.panel1.Controls.Add(this.groupBox3);
            this.panel1.Controls.Add(this.groupBox2);
            this.panel1.Location = new System.Drawing.Point(3, 42);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(771, 422);
            this.panel1.TabIndex = 5;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.ClosedAlertsChBox);
            this.groupBox4.Controls.Add(this.NonCloseAlertsChBox);
            this.groupBox4.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.groupBox4.Location = new System.Drawing.Point(384, 309);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(384, 110);
            this.groupBox4.TabIndex = 6;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Повідомлення:";
            // 
            // ClosedAlertsChBox
            // 
            this.ClosedAlertsChBox.AutoSize = true;
            this.ClosedAlertsChBox.Location = new System.Drawing.Point(47, 51);
            this.ClosedAlertsChBox.Name = "ClosedAlertsChBox";
            this.ClosedAlertsChBox.Size = new System.Drawing.Size(80, 22);
            this.ClosedAlertsChBox.TabIndex = 1;
            this.ClosedAlertsChBox.Text = "Закриті";
            this.ClosedAlertsChBox.UseVisualStyleBackColor = true;
            this.ClosedAlertsChBox.CheckedChanged += new System.EventHandler(this.ClosedAlertsChBox_CheckedChanged);
            // 
            // NonCloseAlertsChBox
            // 
            this.NonCloseAlertsChBox.AutoSize = true;
            this.NonCloseAlertsChBox.Location = new System.Drawing.Point(47, 32);
            this.NonCloseAlertsChBox.Name = "NonCloseAlertsChBox";
            this.NonCloseAlertsChBox.Size = new System.Drawing.Size(102, 22);
            this.NonCloseAlertsChBox.TabIndex = 0;
            this.NonCloseAlertsChBox.Text = "Не закриті";
            this.NonCloseAlertsChBox.UseVisualStyleBackColor = true;
            this.NonCloseAlertsChBox.CheckedChanged += new System.EventHandler(this.NonCloseAlertsChBox_CheckedChanged);
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button1.Location = new System.Drawing.Point(612, 0);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(137, 31);
            this.button1.TabIndex = 5;
            this.button1.Text = "Скинути фільтри";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // Filters
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.FilterBtn);
            this.Name = "Filters";
            this.Size = new System.Drawing.Size(777, 464);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1GuardObj)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2AlarmCateg)).EndInit();
            this.panel1.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button FilterBtn;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.CheckBox SelectAllcheckBox1;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.DataGridView dataGridView1GuardObj;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Column1;
        private System.Windows.Forms.CheckBox TurnOnOffDataTimePicker;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.DataGridView dataGridView2AlarmCateg;
        private System.Windows.Forms.DataGridViewCheckBoxColumn dataGridViewCheckBoxColumn1;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.CheckBox ClosedAlertsChBox;
        private System.Windows.Forms.CheckBox NonCloseAlertsChBox;
        private System.Windows.Forms.DateTimePicker dateTimePicker2;
    }
}
