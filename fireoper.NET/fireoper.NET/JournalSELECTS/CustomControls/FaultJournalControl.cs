﻿using System;
using System.Linq;
using System.Windows.Forms;
using fireoper.NET.SharedClasses;

namespace fireoper.NET.JournalSELECTS.CustomControls
{
    public partial class FaultJournalControl : UserControl
    {
        FireLinqDataContext fl = new FireLinqDataContext(DataAccess.ConnectString);
        //BindingSource bs = new BindingSource();
        IQueryable<alarm_request_journal_view> _alarmsList;
        IQueryable<alarm_action_journal_view> _actionsList;
        DataGrid dataGrid = new DataGrid();

        public FaultJournalControl()
        {
            InitializeComponent();                                    
            //настройки datagridview
            dataGridView1.SelectionMode = DataGridViewSelectionMode.FullRowSelect;           
            Dock = DockStyle.Fill;

            //заповнення журнал заявки
            var AlarmJobJournVar =
                fl.alarm_job_journal_view.Where(c => c.AlarmCategoryId == 2)
                    .OrderByDescending(c => c.ReceivedTime)
                    .Select(c => new AlertsJournalBean
                    {
                        Id = c.Id,
                        ReceivedTime = String.Format("{0:d/M/yyyy HH:mm:ss}", c.ReceivedTime),
                        ClosedTime =
                            (c.ClosedTime == null) ? null : String.Format("{0:d/M/yyyy HH:mm:ss}", c.ClosedTime.Value),
                        ObjectNumber = c.ObjectNumber,
                        ObjectName = c.ObjectName,
                        PremisesName = c.PremisesName,
                        Address = c.Address,
                        ReasonName = c.ReasonName,
                        AlarmCategoryName = c.AlarmCategoryName,
                        RowGuid = c.rowguid
                    });
            dataGridView1.DataSource = AlarmJobJournVar;
            dataGrid.DataSource = AlarmJobJournVar;
            //dataGrid.Dock = DockStyle.Fill;
            //splitContainer1.Panel1.Controls.Add(dataGrid);
            dataGridView1.CellClick += new DataGridViewCellEventHandler(dataGridView1_CellClick);

            //загрузка даних для Сповіщення(datagridview2), Дії(datagridview3)
            if (dataGridView1.Rows.Count < 1) return;
            AlertsJournalBean alertsJournalBean = dataGridView1.Rows[0].DataBoundItem as AlertsJournalBean;
            if (alertsJournalBean != null)
            {
                Guid guid = alertsJournalBean.RowGuid;
                FillSpovischennyaDataGridView(guid);
                FillDiiDataGridView(guid);
            }
        }


        void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (sender is DataGridView && e.RowIndex>-1)
            {
                AlertsJournalBean alertsJournalBean = dataGridView1.Rows[e.RowIndex].DataBoundItem as AlertsJournalBean;
                if (alertsJournalBean != null)
                {
                    Guid guid = alertsJournalBean.RowGuid;
                    if (tabControl1.SelectedIndex == 0)
                    {
                        FillSpovischennyaDataGridView(guid);
                    }
                    else if (tabControl1.SelectedIndex == 1)
                    {
                        FillDiiDataGridView(guid);
                    }
                }
            }
        }

        private void FillDiiDataGridView(Guid guid)
        {
            _actionsList = from c in fl.alarm_action_journal_view
                          where c.AlarmJobJournalGuid == guid
                          select c;
            dataGridView3.DataSource = _actionsList;
        }

        private void FillSpovischennyaDataGridView(Guid guid)
        {
            _alarmsList = from c in fl.alarm_request_journal_view
                         where c.AlarmJobJournalGuid.Value == guid
                         select c;
            dataGridView2.DataSource = _alarmsList;
        }
    }
}
